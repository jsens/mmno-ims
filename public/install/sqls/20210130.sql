-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `tb_auth_group`;
CREATE TABLE `tb_auth_group` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `title` char(100) DEFAULT '',
  `status` tinyint(1) DEFAULT '1',
  `rules` text,
  `menus` text,
  `remark` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tb_auth_group` (`id`, `title`, `status`, `rules`, `menus`, `remark`) VALUES
(17,	'北京',	1,	'',	'1,2,3,52,4,5,6,59,86,7,8,10,60,61,62,12,14,15,29,37,17,18,20,31,32,33,35,42,38,40,41,48,53,43,44,45,57,28,39,89,79,80,76,77,78',	''),
(21,	'上海',	1,	'',	NULL,	''),
(24,	'xxx',	1,	'',	'1,2,3,52,4,5,6,59,86,7,8,10,60,61,62',	''),
(25,	'eee',	1,	NULL,	'1,2,3,52,4,5,6,59,86,7,8,10,60,61,62,12,14,15,29,37,17,18,20,31,32,33,35,42,38,40,41,48,53,43,44,45,57,28,39,89,63,67,65,68,70,71,79,80,76,77,78,82,83,84,85,87,88',	'e');

DROP TABLE IF EXISTS `tb_auth_group_access`;
CREATE TABLE `tb_auth_group_access` (
  `uid` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tb_auth_group_access` (`uid`, `group_id`) VALUES
(11,	18),
(12,	17),
(16,	17),
(17,	17),
(18,	17),
(19,	21),
(27,	17),
(29,	17),
(30,	17),
(31,	17);

DROP TABLE IF EXISTS `tb_auth_rule`;
CREATE TABLE `tb_auth_rule` (
  `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `name` char(80) NOT NULL DEFAULT '',
  `title` char(20) NOT NULL DEFAULT '',
  `type` tinyint(1) NOT NULL DEFAULT '1',
  `status` tinyint(1) NOT NULL DEFAULT '1',
  `condition` char(100) NOT NULL DEFAULT '',
  `group` varchar(50) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tb_auth_rule` (`id`, `pid`, `name`, `title`, `type`, `status`, `condition`, `group`) VALUES
(1,	0,	'admin/admin/listorders',	'排序',	1,	1,	'',	'后台'),
(2,	0,	'admin/configure/product_look',	'产品查看',	1,	1,	'',	'库存配置'),
(3,	0,	'admin/configure/product',	'产品管理',	1,	1,	'',	'库存配置'),
(4,	0,	'admin/configure/product_add',	'产品添加',	1,	1,	'',	'库存配置'),
(5,	0,	'admin/configure/product_edit',	'产品修改',	1,	1,	'',	'库存配置'),
(6,	0,	'admin/configure/product_del',	'产品删除',	1,	1,	'',	'库存配置'),
(7,	0,	'admin/configure/express',	'快递管理',	1,	1,	'',	'库存配置'),
(8,	0,	'admin/configure/express_add',	'快递添加',	1,	1,	'',	'库存配置'),
(9,	0,	'admin/configure/express_edit',	'快递编辑',	1,	1,	'',	'库存配置'),
(10,	0,	'admin/configure/express_delete',	'快递删除',	1,	1,	'',	'库存配置'),
(11,	0,	'admin/configure/unit',	'单位管理',	1,	1,	'',	'库存配置'),
(12,	0,	'admin/configure/unit_add',	'单位添加',	1,	1,	'',	'库存配置'),
(13,	0,	'admin/configure/unit_edit',	'单位编辑',	1,	1,	'',	'库存配置'),
(14,	0,	'admin/configure/unit_delete',	'单位删除',	1,	1,	'',	'库存配置'),
(15,	0,	'admin/configure/product_category',	'产品分类',	1,	1,	'',	'库存配置'),
(16,	0,	'admin/configure/product_category_add',	'产品分类新增',	1,	1,	'',	'库存配置'),
(17,	0,	'admin/configure/product_category_delete',	'产品分类删除',	1,	1,	'',	'库存配置'),
(18,	0,	'admin/configure/product_category_edit',	'产品分类修改',	1,	1,	'',	'库存配置'),
(19,	0,	'admin/configure/warehouse',	'仓库管理',	1,	1,	'',	'库存配置'),
(20,	0,	'admin/configure/warehouse_add',	'仓库新增',	1,	1,	'',	'库存配置'),
(21,	0,	'admin/configure/warehouse_edit',	'仓库修改',	1,	1,	'',	'库存配置'),
(22,	0,	'admin/configure/warehouse_delete',	'仓库删除',	1,	1,	'',	'库存配置'),
(23,	0,	'admin/configure/supplier',	'供应商列表',	1,	1,	'',	'库存配置'),
(24,	0,	'admin/configure/supplier_add',	'供应商新增',	1,	1,	'',	'库存配置'),
(25,	0,	'admin/configure/supplier_edit',	'供应商修改',	1,	1,	'',	'库存配置'),
(26,	0,	'admin/configure/supplier_look',	'供应商查看',	1,	1,	'',	'库存配置'),
(27,	0,	'admin/configure/supplier_delete',	'供应商删除',	1,	1,	'',	'库存配置'),
(28,	0,	'admin/database/export',	'备份数据库',	1,	1,	'',	'数据库'),
(29,	0,	'admin/database/import_list',	'还原列表',	1,	1,	'',	'数据库'),
(30,	0,	'admin/database/export_list',	'备份列表',	1,	1,	'',	'数据库'),
(31,	0,	'admin/database/optimize',	'优化表',	1,	1,	'',	'数据库'),
(32,	0,	'admin/database/repair',	'修复表',	1,	1,	'',	'数据库'),
(33,	0,	'admin/database/del',	'删除备份文件',	1,	1,	'',	'数据库'),
(34,	0,	'admin/database/import',	'还原数据库',	1,	1,	'',	'数据库'),
(35,	0,	'admin/finance/bank',	'银行管理',	1,	1,	'',	'财务'),
(36,	0,	'admin/finance/bank_add',	'新增银行',	1,	1,	'',	'财务'),
(37,	0,	'admin/finance/bank_delete',	'删除银行',	1,	1,	'',	'财务'),
(38,	0,	'admin/finance/bank_edit',	'修改银行',	1,	1,	'',	'财务'),
(39,	0,	'admin/finance/category',	'财务分类',	1,	1,	'',	'财务'),
(40,	0,	'admin/finance/category_add',	'新增财务分类',	1,	1,	'',	'财务'),
(41,	0,	'admin/finance/category_delete',	'财务银行分类',	1,	1,	'',	'财务'),
(42,	0,	'admin/finance/category_edit',	'修改财务分类',	1,	1,	'',	'财务'),
(43,	0,	'admin/finance/add',	'新增财务',	1,	1,	'',	'财务'),
(44,	0,	'admin/finance/query',	'账务查询',	1,	1,	'',	'财务'),
(45,	0,	'admin/finance/query_delete',	'撤销账单',	1,	1,	'',	'财务'),
(46,	0,	'admin/production/product_build_undo',	'生产撤销',	1,	1,	'',	'生产'),
(47,	0,	'admin/production/product_build_query',	'加工记录',	1,	1,	'',	'生产'),
(48,	0,	'admin/production/product_build_submit',	'产品加工提交',	1,	1,	'',	'生产'),
(49,	0,	'admin/production/product_build',	'产品加工',	1,	1,	'',	'生产'),
(50,	0,	'admin/production/product_relation',	'产品关系',	1,	1,	'',	'生产'),
(51,	0,	'admin/production/product_relation_edit',	'产品关系编辑',	1,	1,	'',	'生产'),
(52,	0,	'admin/production/product_relation_edit_submit',	'产品关联提交',	1,	1,	'',	'生产'),
(53,	0,	'admin/index/log_clear',	'日志删除',	1,	1,	'',	'控制台'),
(54,	0,	'admin/index/password',	'修改自己的密码',	1,	1,	'',	'控制台'),
(55,	0,	'admin/index/index',	'框架页面',	1,	1,	'',	'控制台'),
(56,	0,	'admin/index/main',	'首页',	1,	1,	'',	'控制台'),
(57,	0,	'admin/index/log',	'我的日志',	1,	1,	'',	'控制台'),
(58,	0,	'admin/member/group_price',	'会员组销价管理',	1,	1,	'',	'会员'),
(59,	0,	'admin/member/index',	'会员管理',	1,	1,	'',	'会员'),
(60,	0,	'admin/member/delete',	'删除管理',	1,	1,	'',	'会员'),
(61,	0,	'admin/member/look',	'查看会员',	1,	1,	'',	'会员'),
(62,	0,	'admin/member/edit',	'修改会员',	1,	1,	'',	'会员'),
(63,	0,	'admin/member/add',	'新增会员',	1,	1,	'',	'会员'),
(64,	0,	'admin/member/group',	'会员分组',	1,	1,	'',	'会员'),
(65,	0,	'admin/member/group_add',	'会员分组新增',	1,	1,	'',	'会员'),
(66,	0,	'admin/member/group_edit',	'会员分组修改',	1,	1,	'',	'会员'),
(67,	0,	'admin/member/group_delete',	'会员分组删除',	1,	1,	'',	'会员'),
(68,	0,	'admin/json/finance_category',	'财务分类',	1,	1,	'',	'JSON'),
(69,	0,	'admin/json/menu',	'菜单',	1,	1,	'',	'JSON'),
(70,	0,	'admin/json/city',	'城市',	1,	1,	'',	'JSON'),
(71,	0,	'admin/json/product',	'产品',	1,	1,	'',	'JSON'),
(72,	0,	'admin/json/member',	'会员',	1,	1,	'',	'JSON'),
(73,	0,	'admin/system/auth_group',	'信息列表',	1,	1,	'',	'系统'),
(74,	0,	'admin/system/auth_group_add',	'添加角色',	1,	1,	'',	'系统'),
(75,	0,	'admin/system/auth_group_edit',	'编辑角色',	1,	1,	'',	'系统'),
(76,	0,	'admin/system/auth_group_delete',	'删除资源',	1,	1,	'',	'系统'),
(77,	0,	'admin/system/auth_rule',	'显示资源列表',	1,	1,	'',	'系统'),
(78,	0,	'admin/system/node_parse',	'节点解析',	1,	1,	'',	'系统'),
(79,	0,	'admin/system/node_refresh',	'刷新节点',	1,	1,	'',	'系统'),
(80,	0,	'admin/system/user',	'列表',	1,	1,	'',	'系统'),
(81,	0,	'admin/system/user_add',	'添加用户',	1,	1,	'',	'系统'),
(82,	0,	'admin/system/user_edit',	'编辑用户',	1,	1,	'',	'系统'),
(83,	0,	'admin/system/user_delete',	'用户删除',	1,	1,	'',	'系统'),
(84,	0,	'admin/system/menu',	'菜单列表',	1,	1,	'',	'系统'),
(85,	0,	'admin/system/menu_add',	'添加',	1,	1,	'',	'系统'),
(86,	0,	'admin/system/menu_edit',	'编辑菜单',	1,	1,	'',	'系统'),
(87,	0,	'admin/system/menu_delete',	'删除菜单',	1,	1,	'',	'系统'),
(88,	0,	'admin/system/config',	'配置列表',	1,	1,	'',	'系统'),
(89,	0,	'admin/inventory/storage',	'入库',	1,	1,	'',	'库存管理'),
(90,	0,	'admin/inventory/storage_submit',	'入库提交',	1,	1,	'',	'库存管理'),
(91,	0,	'admin/inventory/storage_undo',	'入库撤消',	1,	1,	'',	'库存管理'),
(92,	0,	'admin/inventory/storage_query',	'入库查询',	1,	1,	'',	'库存管理'),
(93,	0,	'admin/inventory/sales',	'出库',	1,	1,	'',	'库存管理'),
(94,	0,	'admin/inventory/sales_submit',	'出库提交',	1,	1,	'',	'库存管理'),
(95,	0,	'admin/inventory/sales_query',	'出库查询',	1,	1,	'',	'库存管理'),
(96,	0,	'admin/inventory/sales_undo',	'产品出库撤消',	1,	1,	'',	'库存管理'),
(97,	0,	'admin/inventory/sales_returns_query',	'退货查询',	1,	1,	'',	'库存管理'),
(98,	0,	'admin/inventory/sales_returns_add',	'出库退货提交',	1,	1,	'',	'库存管理'),
(99,	0,	'admin/inventory/sales_look',	'产品出库查询',	1,	1,	'',	'库存管理'),
(100,	0,	'admin/inventory/sales_look_info_update',	'补充快递信息',	1,	1,	'',	'库存管理'),
(101,	0,	'admin/inventory/stock_delete',	'库存记录删除',	1,	1,	'',	'库存管理'),
(102,	0,	'admin/inventory/stock_query',	'库存查询',	1,	1,	'',	'库存管理'),
(103,	0,	'admin/inventory/transfer_add',	'库存调拨窗口',	1,	1,	'',	'库存管理'),
(104,	0,	'admin/inventory/transfer_query',	'调拨查询',	1,	1,	'',	'库存管理'),
(105,	0,	'admin/inventory/scrapped_add',	'报废窗口',	1,	1,	'',	'库存管理'),
(106,	0,	'admin/inventory/scrapped_query',	'报废查询',	1,	1,	'',	'库存管理'),
(107,	0,	'admin/prints/orders_view',	'出库订单详情',	1,	1,	'',	'打印'),
(108,	0,	'admin/prints/orders_list',	'出库订单列表',	1,	1,	'',	'打印'),
(109,	0,	'admin/prints/storage_list',	'入库查询',	1,	1,	'',	'打印'),
(110,	0,	'admin/prints/storage_view',	'入库查询',	1,	1,	'',	'打印'),
(111,	0,	'admin/prints/finance_list',	'打印账务',	1,	1,	'',	'打印'),
(112,	0,	'admin/everyone/login',	'用户登录',	1,	1,	'',	'公共'),
(113,	0,	'admin/everyone/logout',	'用户登出',	1,	1,	'',	'公共');

DROP TABLE IF EXISTS `tb_express`;
CREATE TABLE `tb_express` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tb_express` (`id`, `name`, `sort`) VALUES
(1,	'顺丰',	NULL),
(2,	'中通',	NULL),
(3,	'圆通',	NULL);

DROP TABLE IF EXISTS `tb_finance_accounts`;
CREATE TABLE `tb_finance_accounts` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL COMMENT '用户',
  `bank_id` int(11) DEFAULT NULL COMMENT '银行',
  `c_id` int(11) DEFAULT NULL COMMENT '分类',
  `status` int(11) DEFAULT '1' COMMENT '状态',
  `type` int(11) DEFAULT NULL COMMENT '收入支出类型',
  `money` double(11,2) DEFAULT NULL COMMENT '金额',
  `datetime` int(11) DEFAULT NULL COMMENT '日期时间',
  `attn_id` int(11) DEFAULT NULL COMMENT '经办人',
  `remark` text COMMENT '备注',
  `create_time` int(11) DEFAULT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='账务';

INSERT INTO `tb_finance_accounts` (`id`, `u_id`, `bank_id`, `c_id`, `status`, `type`, `money`, `datetime`, `attn_id`, `remark`, `create_time`) VALUES
(1,	1,	1,	0,	1,	0,	111.00,	1557237780,	30,	'1111',	1559052188),
(3,	1,	1,	3,	1,	1,	11111.00,	1611983160,	1,	'',	1611983208);

DROP TABLE IF EXISTS `tb_finance_bank`;
CREATE TABLE `tb_finance_bank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '名称',
  `money` double(11,2) DEFAULT NULL COMMENT '金额',
  `default` int(1) DEFAULT '0' COMMENT '是否默认',
  `remark` varchar(255) DEFAULT NULL COMMENT '备注',
  `sort` int(11) DEFAULT NULL COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='财务银行';

INSERT INTO `tb_finance_bank` (`id`, `name`, `money`, `default`, `remark`, `sort`) VALUES
(1,	'1111',	12111.00,	1,	'11111',	0);

DROP TABLE IF EXISTS `tb_finance_category`;
CREATE TABLE `tb_finance_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `type` int(11) DEFAULT NULL,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产器分类';

INSERT INTO `tb_finance_category` (`id`, `name`, `pid`, `type`, `sort`) VALUES
(1,	'收入',	0,	0,	1),
(2,	'支出',	0,	0,	2),
(3,	'xxxx',	0,	1,	3);

DROP TABLE IF EXISTS `tb_member`;
CREATE TABLE `tb_member` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `g_id` int(11) DEFAULT NULL COMMENT '会员分组',
  `card` varchar(255) DEFAULT NULL COMMENT '会员卡号',
  `nickname` varchar(255) DEFAULT NULL COMMENT '会员姓名',
  `sex` int(11) DEFAULT NULL,
  `tel` varchar(255) DEFAULT NULL COMMENT '电话',
  `qq` varchar(255) DEFAULT NULL COMMENT 'qq',
  `email` varchar(255) DEFAULT NULL COMMENT '邮箱',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `id_card` varchar(255) DEFAULT NULL COMMENT '身份证号码',
  `birthday` varchar(20) DEFAULT NULL COMMENT '生日',
  `remark` text COMMENT '备注',
  `create_time` int(11) DEFAULT NULL,
  `points` bigint(20) DEFAULT '0' COMMENT '积分',
  `update` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员';

INSERT INTO `tb_member` (`id`, `u_id`, `g_id`, `card`, `nickname`, `sex`, `tel`, `qq`, `email`, `address`, `id_card`, `birthday`, `remark`, `create_time`, `points`, `update`, `update_time`) VALUES
(3,	1,	0,	'',	'111',	1,	'',	'',	'',	'',	'',	'',	'',	1611980347,	0,	NULL,	NULL),
(4,	31,	0,	'',	'xxxx',	1,	'',	'',	'',	'',	'',	'',	'',	1611990539,	0,	NULL,	NULL),
(5,	31,	1,	'1111222233334444',	'vvv',	1,	'18911112222',	'110111222',	'bjphper@qq.com',	'北京西城',	'33098776554334',	'2012-01-30',	'主要VIP',	1611990572,	154,	31,	1611991713);

DROP TABLE IF EXISTS `tb_member_card`;
CREATE TABLE `tb_member_card` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `card_number` varchar(255) DEFAULT NULL COMMENT '会员卡号',
  `status` int(1) DEFAULT NULL,
  `time` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='会员卡';


DROP TABLE IF EXISTS `tb_member_group`;
CREATE TABLE `tb_member_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `sort` int(11) DEFAULT NULL,
  `discounts` double(3,2) DEFAULT '0.00' COMMENT '折扣',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产器分类';

INSERT INTO `tb_member_group` (`id`, `name`, `pid`, `sort`, `discounts`) VALUES
(1,	'一级代理',	0,	0,	0.00),
(2,	'二级代理',	0,	0,	0.00),
(3,	'三级代理',	0,	0,	0.00),
(4,	'四级代理',	0,	0,	0.00);

DROP TABLE IF EXISTS `tb_member_points`;
CREATE TABLE `tb_member_points` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `member` int(11) DEFAULT NULL,
  `m_id` int(11) DEFAULT NULL,
  `type` int(1) DEFAULT '0',
  `create_time` int(11) DEFAULT NULL,
  `value` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='积分日志';

INSERT INTO `tb_member_points` (`id`, `u_id`, `member`, `m_id`, `type`, `create_time`, `value`, `title`) VALUES
(1,	31,	NULL,	5,	1,	1611991123,	11,	'出库积分'),
(2,	31,	NULL,	5,	1,	1611991124,	11,	'出库积分'),
(3,	31,	NULL,	5,	1,	1611991124,	11,	'出库积分'),
(4,	31,	NULL,	5,	1,	1611991124,	11,	'出库积分'),
(5,	31,	NULL,	5,	1,	1611991124,	11,	'出库积分'),
(6,	31,	NULL,	5,	1,	1611991124,	11,	'出库积分'),
(7,	31,	NULL,	5,	1,	1611991124,	11,	'出库积分'),
(8,	31,	NULL,	5,	1,	1611991125,	11,	'出库积分'),
(9,	31,	NULL,	5,	1,	1611991125,	11,	'出库积分'),
(10,	31,	NULL,	5,	1,	1611991125,	11,	'出库积分'),
(11,	31,	NULL,	5,	1,	1611991125,	11,	'出库积分'),
(12,	31,	NULL,	5,	1,	1611991400,	11,	'出库积分'),
(13,	31,	NULL,	5,	1,	1611991689,	22,	'出库积分'),
(14,	31,	NULL,	5,	1,	1611991689,	22,	'出库积分'),
(15,	31,	NULL,	5,	1,	1611991689,	22,	'出库积分'),
(16,	31,	NULL,	5,	1,	1611991690,	22,	'出库积分'),
(17,	31,	NULL,	5,	1,	1611991690,	22,	'出库积分'),
(18,	31,	NULL,	5,	1,	1611991690,	22,	'出库积分'),
(19,	31,	NULL,	5,	1,	1611991691,	22,	'出库积分');

DROP TABLE IF EXISTS `tb_member_price`;
CREATE TABLE `tb_member_price` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) DEFAULT NULL,
  `g_id` int(11) DEFAULT NULL,
  `price` double(20,2) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tb_member_price` (`id`, `p_id`, `g_id`, `price`) VALUES
(1,	1,	1,	1.00),
(2,	1,	2,	22.00),
(3,	2,	1,	1.00),
(4,	2,	2,	22.00),
(5,	3,	1,	2.00),
(6,	3,	2,	2.00),
(7,	4,	1,	1.00),
(8,	4,	2,	2.00),
(9,	4,	3,	3.00),
(10,	4,	4,	4.00),
(11,	6,	1,	22.00),
(12,	6,	2,	0.00),
(13,	6,	3,	0.00),
(14,	6,	4,	0.00),
(15,	7,	1,	22.00),
(16,	7,	2,	0.00),
(17,	7,	3,	0.00),
(18,	7,	4,	0.00),
(19,	8,	1,	22.00),
(20,	8,	2,	0.00),
(21,	8,	3,	0.00),
(22,	8,	4,	0.00),
(23,	9,	1,	22.00),
(24,	9,	2,	0.00),
(25,	9,	3,	0.00),
(26,	9,	4,	0.00);

DROP TABLE IF EXISTS `tb_operate`;
CREATE TABLE `tb_operate` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';


DROP TABLE IF EXISTS `tb_operate_1`;
CREATE TABLE `tb_operate_1` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';


DROP TABLE IF EXISTS `tb_operate_10`;
CREATE TABLE `tb_operate_10` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';


DROP TABLE IF EXISTS `tb_operate_2`;
CREATE TABLE `tb_operate_2` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';

INSERT INTO `tb_operate_2` (`id`, `u_id`, `title`, `status`, `create_time`, `ip`, `client`, `country`, `area`, `url`, `data`, `log`) VALUES
(50,	1,	'成功登录系统',	1,	1559030183,	'171.88.1.34',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(49,	1,	'成功登录系统',	1,	1559029021,	'1.204.205.59',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(47,	1,	'成功登录系统',	1,	1559026552,	'182.117.176.27',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(48,	1,	'成功登录系统',	1,	1559028761,	'125.62.5.96',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(46,	1,	'成功登录系统',	1,	1559026292,	'27.187.170.93',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(45,	1,	'入库成功',	1,	1559026289,	'115.56.39.127',	'pc',	'',	'',	'/admin/inventory/storage_submit',	'a:9:{s:4:\"code\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:1;s:1:\"1\";}s:9:\"warehouse\";a:1:{i:1;s:1:\"2\";}s:16:\"product_quantity\";a:1:{i:1;s:2:\"20\";}s:11:\"group_price\";a:1:{i:1;s:3:\"100\";}s:8:\"supplier\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(44,	1,	'产品关联提交',	1,	1559026223,	'115.56.39.127',	'pc',	'',	'',	'/admin/production/product_relation_edit_submit?id=2',	'a:2:{s:4:\"code\";s:0:\"\";s:11:\"product_ids\";a:1:{i:0;s:0:\"\";}}',	''),
(42,	1,	'成功登录系统',	1,	1559025791,	'171.8.254.201',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(43,	1,	'成功登录系统',	1,	1559026123,	'115.56.39.127',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(41,	1,	'打印出货单=>20190528311257263831',	1,	1559025691,	'223.72.67.182',	'pc',	'',	'',	'/admin/prints/orders_view?id=1&session_id=t58fd8lbt540l3gjue17im21ff&r=0.19589730460150578',	'a:0:{}',	''),
(39,	1,	'成功登录系统',	1,	1559024773,	'112.86.69.48',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(40,	1,	'成功登录系统',	1,	1559025516,	'223.72.67.182',	'mobile',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(38,	1,	'打印账务单',	1,	1559024352,	'113.200.156.117',	'pc',	'',	'',	'/admin/prints/finance_list?session_id=2651m7ti8f2c93007vqon8tlkg&r=0.2695308762426796&timea=2019-04-28&timeb=2019-05-28&b_id=&attn_id=&remark=',	'a:0:{}',	''),
(36,	1,	'成功登录系统',	1,	1559023582,	'183.3.155.73',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(37,	1,	'成功登录系统',	1,	1559024267,	'113.200.156.117',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(33,	1,	'成功登录系统',	1,	1559023511,	'175.169.111.129',	'mobile',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(34,	1,	'成功登录系统',	1,	1559023562,	'113.104.244.35',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(35,	1,	'成功登录系统',	1,	1559023575,	'175.169.111.129',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(32,	1,	'成功登录系统',	1,	1559021939,	'61.148.245.73',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(51,	1,	'成功登录系统',	1,	1559031293,	'58.246.20.210',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(52,	1,	'成功登录系统',	1,	1559031529,	'14.114.37.90',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(53,	1,	'成功登录系统',	1,	1559031736,	'119.90.88.106',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(54,	1,	'成功登录系统',	1,	1559032055,	'211.97.10.43',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(55,	1,	'成功登录系统',	1,	1559033016,	'220.173.124.78',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(56,	1,	'成功登录系统',	1,	1559034469,	'113.109.110.67',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(57,	1,	'成功登录系统',	1,	1559034575,	'222.211.206.46',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(58,	1,	'入库撤消',	1,	1559034807,	'222.211.206.46',	'pc',	'',	'',	'/admin/inventory/storage_undo?id=4',	'a:0:{}',	''),
(59,	1,	'成功登录系统',	1,	1559035309,	'110.184.163.99',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(60,	1,	'成功登录系统',	1,	1559040723,	'175.188.64.228',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(61,	1,	'成功登录系统',	1,	1559048766,	'180.106.15.242',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(62,	1,	'成功登录系统',	1,	1559048960,	'183.17.67.146',	'mobile',	'',	'',	'/admin/everyone/login',	'a:2:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";}',	''),
(63,	1,	'成功登录系统',	1,	1559049000,	'180.106.15.242',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(64,	1,	'用户密码错误',	0,	1559049113,	'113.65.210.18',	'mobile',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:5:\"admin\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(65,	1,	'成功登录系统',	1,	1559049133,	'113.65.210.18',	'mobile',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(66,	1,	'成功登录系统',	1,	1559049424,	'124.114.249.125',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(67,	1,	'成功登录系统',	1,	1559050091,	'180.126.42.76',	'mobile',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(68,	1,	'成功登录系统',	1,	1559050094,	'112.86.69.48',	'mobile',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(69,	1,	'成功登录系统',	1,	1559050260,	'112.86.69.48',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(70,	1,	'成功登录系统',	1,	1559051010,	'61.140.206.221',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(71,	1,	'产品关联提交',	1,	1559051027,	'61.140.206.221',	'pc',	'',	'',	'/admin/production/product_relation_edit_submit?id=3',	'a:2:{s:4:\"code\";s:0:\"\";s:11:\"product_ids\";a:1:{i:0;s:0:\"\";}}',	''),
(72,	1,	'成功登录系统',	1,	1559051354,	'59.109.152.230',	'mobile',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(73,	1,	'成功登录系统',	1,	1559052042,	'223.72.57.23',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(74,	1,	'新增银行',	1,	1559052175,	'223.72.57.23',	'pc',	'',	'',	'/admin/finance/bank_add',	'a:3:{s:4:\"name\";s:4:\"1111\";s:5:\"money\";s:4:\"1111\";s:6:\"remark\";s:5:\"11111\";}',	''),
(75,	1,	'新增账务',	1,	1559052188,	'223.72.57.23',	'pc',	'',	'',	'/admin/finance/add',	'a:7:{s:4:\"type\";s:12:\"选择类型\";s:4:\"c_id\";s:12:\"选择分类\";s:7:\"bank_id\";s:1:\"1\";s:5:\"money\";s:3:\"111\";s:8:\"datetime\";s:16:\"2019-05-07 22:03\";s:7:\"attn_id\";s:2:\"30\";s:6:\"remark\";s:4:\"1111\";}',	''),
(76,	1,	'成功登录系统',	1,	1559053687,	'110.52.105.173',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(77,	1,	'成功登录系统',	1,	1559054144,	'119.60.64.116',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(78,	1,	'库存调拨',	1,	1559054313,	'119.60.64.116',	'pc',	'',	'',	'/admin/inventory/transfer_add?id=1',	'a:3:{s:9:\"warehouse\";s:1:\"1\";s:6:\"number\";s:1:\"2\";s:6:\"remark\";s:0:\"\";}',	''),
(79,	1,	'新增供应商',	1,	1559054423,	'119.60.64.116',	'pc',	'',	'',	'/admin/configure/supplier_add',	'a:9:{s:7:\"company\";s:2:\"ce\";s:4:\"name\";s:0:\"\";s:3:\"tel\";s:0:\"\";s:3:\"fax\";s:0:\"\";s:6:\"mobile\";s:0:\"\";s:4:\"site\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(80,	1,	'修改供应商',	1,	1559054461,	'119.60.64.116',	'pc',	'',	'',	'/admin/configure/supplier_edit',	'a:10:{s:2:\"id\";s:1:\"4\";s:7:\"company\";s:6:\"测试\";s:4:\"name\";s:12:\"企业返现\";s:3:\"tel\";s:7:\"1111111\";s:3:\"fax\";s:0:\"\";s:6:\"mobile\";s:0:\"\";s:4:\"site\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(81,	1,	'成功登录系统',	1,	1559056288,	'180.106.15.242',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(82,	1,	'成功登录系统',	1,	1559056391,	'122.192.12.103',	'mobile',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(83,	1,	'成功登录系统',	1,	1559056423,	'223.104.27.232',	'mobile',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(84,	1,	'成功登录系统',	1,	1559060555,	'119.86.37.174',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(85,	1,	'成功登录系统',	1,	1559062272,	'218.87.25.149',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(86,	1,	'成功登录系统',	1,	1559084994,	'183.52.198.196',	'mobile',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(87,	1,	'成功登录系统',	1,	1559091418,	'121.231.46.186',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(88,	1,	'成功登录系统',	1,	1559092386,	'59.61.92.150',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(89,	1,	'成功登录系统',	1,	1559092401,	'180.106.15.242',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(90,	1,	'成功登录系统',	1,	1559093783,	'1.202.124.123',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(91,	1,	'成功登录系统',	1,	1559093800,	'1.202.124.123',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(92,	1,	'成功登录系统',	1,	1559095757,	'111.160.91.250',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(93,	1,	'成功登录系统',	1,	1559095777,	'111.160.91.250',	'pc',	'',	'',	'/admin/everyone/login',	'a:2:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";}',	''),
(94,	1,	'成功登录系统',	1,	1559096761,	'118.250.172.63',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(95,	1,	'成功登录系统',	1,	1559096903,	'125.118.132.23',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(96,	1,	'入库成功',	1,	1559097009,	'125.118.132.23',	'pc',	'',	'',	'/admin/inventory/storage_submit',	'a:9:{s:4:\"code\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:1;s:1:\"1\";}s:9:\"warehouse\";a:1:{i:1;s:1:\"2\";}s:16:\"product_quantity\";a:1:{i:1;s:1:\"2\";}s:11:\"group_price\";a:1:{i:1;s:1:\"1\";}s:8:\"supplier\";s:1:\"3\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:3:\"123\";}',	''),
(97,	1,	'销售产品',	1,	1559097054,	'125.118.132.23',	'pc',	'',	'',	'/admin/inventory/sales_submit',	'a:12:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:1;s:1:\"1\";}s:17:\"product_warehouse\";a:1:{i:1;s:1:\"2\";}s:16:\"product_quantity\";a:1:{i:1;s:1:\"1\";}s:11:\"group_price\";a:1:{i:1;s:1:\"1\";}s:9:\"ship_time\";s:16:\"2019-05-29 10:30\";s:10:\"express_id\";s:1:\"1\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:5:\"21132\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:3:\"123\";}',	''),
(98,	1,	'入库成功',	1,	1559097091,	'125.118.132.23',	'pc',	'',	'',	'/admin/inventory/storage_submit',	'a:9:{s:4:\"code\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:1;s:1:\"1\";}s:9:\"warehouse\";a:1:{i:1;s:1:\"2\";}s:16:\"product_quantity\";a:1:{i:1;s:1:\"1\";}s:11:\"group_price\";a:1:{i:1;s:1:\"1\";}s:8:\"supplier\";s:0:\"\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(99,	1,	'销售产品',	1,	1559097112,	'125.118.132.23',	'pc',	'',	'',	'/admin/inventory/sales_submit',	'a:12:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:1;s:1:\"1\";}s:17:\"product_warehouse\";a:1:{i:1;s:1:\"2\";}s:16:\"product_quantity\";a:1:{i:1;s:1:\"4\";}s:11:\"group_price\";a:1:{i:1;s:1:\"1\";}s:9:\"ship_time\";s:16:\"2019-05-29 10:31\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(100,	1,	'成功登录系统',	1,	1559097354,	'183.239.207.144',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(101,	1,	'成功登录系统',	1,	1559097687,	'118.187.58.216',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(102,	1,	'成功登录系统',	1,	1559098166,	'223.72.67.182',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(103,	1,	'成功登录系统',	1,	1559099051,	'113.70.58.77',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(104,	1,	'成功登录系统',	1,	1559099149,	'222.214.45.197',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(105,	1,	'成功登录系统',	1,	1611921745,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/everyone/login',	'a:2:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"111111\";}',	''),
(106,	1,	'新增产品',	1,	1611932729,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:11:{s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:4:\"name\";s:6:\"牙刷\";s:4:\"code\";s:3:\"111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"11\";s:8:\"purchase\";s:2:\"22\";s:6:\"remark\";s:0:\"\";}',	''),
(107,	1,	'更新产品',	1,	1611933130,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_edit',	'a:14:{s:2:\"id\";s:1:\"4\";s:12:\"http_referer\";s:63:\"http://127.0.0.1/_mmno_/mmno_ims/public/admin/configure/product\";s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:4:\"name\";s:6:\"牙刷\";s:4:\"code\";s:3:\"111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"11\";s:8:\"purchase\";s:2:\"22\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(108,	1,	'新增产品',	1,	1611933266,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:7:\"包材1\";s:4:\"code\";s:4:\"1111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"11\";s:8:\"purchase\";s:2:\"22\";s:4:\"type\";s:1:\"2\";s:6:\"remark\";s:0:\"\";}',	''),
(109,	1,	'产品关联提交',	1,	1611933285,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/production/product_relation_edit_submit?id=4',	'a:3:{s:4:\"code\";s:0:\"\";s:11:\"product_ids\";a:2:{i:0;s:0:\"\";i:5;s:1:\"5\";}s:8:\"multiple\";a:1:{i:5;s:2:\"10\";}}',	''),
(110,	1,	'入库成功',	1,	1611933419,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/storage_submit',	'a:10:{s:4:\"code\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:4:\"5000\";}s:11:\"group_price\";a:1:{i:4;s:2:\"22\";}s:9:\"warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"110.00\";s:8:\"supplier\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(111,	1,	'入库成功',	1,	1611933596,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/storage_submit',	'a:10:{s:4:\"code\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:4:\"1000\";}s:11:\"group_price\";a:1:{i:4;s:2:\"22\";}s:9:\"warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"22.00\";s:8:\"supplier\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(112,	1,	'销售产品',	1,	1611933607,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-29 23:19\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(113,	1,	'新增财务分类',	1,	1611934267,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/finance/category_add',	'a:3:{s:4:\"type\";s:1:\"0\";s:3:\"pid\";s:0:\"\";s:4:\"name\";s:6:\"收入\";}',	''),
(114,	1,	'新增财务分类',	1,	1611934280,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/finance/category_add',	'a:3:{s:4:\"type\";s:1:\"0\";s:3:\"pid\";s:0:\"\";s:4:\"name\";s:6:\"支出\";}',	''),
(115,	1,	'成功登录系统',	1,	1611968959,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/everyone/login',	'a:2:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"111111\";}',	''),
(116,	1,	'入库成功',	1,	1611975264,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/storage_submit',	'a:10:{s:4:\"code\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"22\";}s:9:\"warehouse\";a:1:{i:4;s:1:\"1\";}s:6:\"amount\";s:4:\"0.00\";s:8:\"supplier\";s:0:\"\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(117,	1,	'报废产品',	1,	1611975902,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/scrapped_add?id=3',	'a:2:{s:8:\"quantity\";s:1:\"2\";s:6:\"remark\";s:0:\"\";}',	''),
(118,	1,	'报废产品',	1,	1611975916,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/scrapped_add?id=1',	'a:2:{s:8:\"quantity\";s:4:\"2996\";s:6:\"remark\";s:0:\"\";}',	''),
(119,	1,	'入库成功',	1,	1611975939,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/storage_submit',	'a:10:{s:4:\"code\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:5;s:1:\"5\";}s:16:\"product_quantity\";a:1:{i:5;s:5:\"11111\";}s:11:\"group_price\";a:1:{i:5;s:2:\"22\";}s:9:\"warehouse\";a:1:{i:5;s:1:\"2\";}s:6:\"amount\";s:9:\"244442.00\";s:8:\"supplier\";s:0:\"\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(120,	1,	'生产成功',	1,	1611975966,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/production/product_build_submit',	'a:8:{s:4:\"code\";s:0:\"\";s:10:\"product_id\";s:1:\"4\";s:11:\"product_ids\";a:1:{i:5;s:1:\"5\";}s:17:\"product_warehouse\";a:1:{i:5;s:1:\"2\";}s:10:\"build_time\";s:16:\"2021-01-30 11:05\";s:8:\"quantity\";s:1:\"5\";s:12:\"warehouse_id\";s:1:\"3\";s:6:\"remark\";s:0:\"\";}',	''),
(121,	1,	'修改仓库',	1,	1611980130,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/warehouse_edit',	'a:5:{s:2:\"id\";s:1:\"3\";s:4:\"name\";s:12:\"退货仓库\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:4:\"uids\";a:1:{i:0;s:1:\"1\";}}',	''),
(122,	1,	'修改仓库',	1,	1611980134,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/warehouse_edit',	'a:6:{s:2:\"id\";s:1:\"3\";s:4:\"name\";s:12:\"退货仓库\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:7:\"default\";a:1:{i:0;s:1:\"1\";}s:4:\"uids\";a:1:{i:0;s:1:\"1\";}}',	''),
(123,	1,	'修改仓库',	1,	1611980137,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/warehouse_edit',	'a:6:{s:2:\"id\";s:1:\"3\";s:4:\"name\";s:12:\"退货仓库\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:7:\"default\";a:1:{i:0;s:1:\"1\";}s:4:\"uids\";a:1:{i:0;s:1:\"1\";}}',	''),
(124,	1,	'销售产品',	1,	1611980297,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 12:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(125,	1,	'销售产品',	1,	1611980301,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 12:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(126,	1,	'销售产品',	1,	1611980306,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 12:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(127,	1,	'销售产品',	1,	1611980310,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 12:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(128,	1,	'销售产品',	1,	1611980315,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:5;s:1:\"5\";}s:16:\"product_quantity\";a:1:{i:5;s:1:\"1\";}s:11:\"group_price\";a:1:{i:5;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:5;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 12:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(129,	1,	'销售产品',	1,	1611980319,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 12:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(130,	1,	'销售产品',	1,	1611980323,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 12:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(131,	1,	'销售产品',	1,	1611980328,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 12:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(132,	1,	'销售产品',	1,	1611980334,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 12:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(133,	1,	'销售产品',	1,	1611980340,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:0:\"\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 12:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(134,	1,	'新增会员',	1,	1611980347,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/member/add',	'a:11:{s:4:\"g_id\";s:0:\"\";s:8:\"nickname\";s:3:\"111\";s:3:\"sex\";s:1:\"1\";s:4:\"card\";s:0:\"\";s:3:\"tel\";s:0:\"\";s:2:\"qq\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"address\";s:0:\"\";s:7:\"id_card\";s:0:\"\";s:8:\"birthday\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(135,	1,	'新增产品',	1,	1611980399,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"2\";s:5:\"image\";s:23:\"image/6014de5900a5f.jpg\";s:4:\"name\";s:2:\"11\";s:4:\"code\";s:2:\"11\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"11\";s:8:\"purchase\";s:2:\"11\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(136,	1,	'新增产品',	1,	1611980417,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"2\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:3:\"xxx\";s:4:\"code\";s:4:\"1122\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"1\";s:8:\"purchase\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(137,	1,	'新增产品',	1,	1611980451,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"2\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:2:\"22\";s:4:\"code\";s:2:\"22\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"2\";s:8:\"purchase\";s:1:\"2\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(138,	1,	'新增产品',	1,	1611980463,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/5fd32c46276a4.jpg\";s:4:\"name\";s:2:\"33\";s:4:\"code\";s:2:\"33\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"33\";s:8:\"purchase\";s:2:\"33\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(139,	1,	'修改供应商',	1,	1611980775,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/supplier_edit',	'a:10:{s:2:\"id\";s:1:\"4\";s:7:\"company\";s:6:\"测试\";s:4:\"name\";s:12:\"企业返现\";s:3:\"tel\";s:7:\"1111111\";s:3:\"fax\";s:0:\"\";s:6:\"mobile\";s:0:\"\";s:4:\"site\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(140,	1,	'修改供应商',	1,	1611980781,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/supplier_edit',	'a:10:{s:2:\"id\";s:1:\"3\";s:7:\"company\";s:9:\"苹果园\";s:4:\"name\";s:3:\"111\";s:3:\"tel\";s:3:\"222\";s:3:\"fax\";s:0:\"\";s:6:\"mobile\";s:0:\"\";s:4:\"site\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(141,	1,	'修改供应商',	1,	1611980785,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/supplier_edit',	'a:10:{s:2:\"id\";s:1:\"2\";s:7:\"company\";s:15:\"防伪标厂家\";s:4:\"name\";s:3:\"222\";s:3:\"tel\";s:3:\"222\";s:3:\"fax\";s:0:\"\";s:6:\"mobile\";s:0:\"\";s:4:\"site\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(142,	1,	'修改供应商',	1,	1611980791,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/supplier_edit',	'a:10:{s:2:\"id\";s:1:\"1\";s:7:\"company\";s:15:\"包装盒厂家\";s:4:\"name\";s:3:\"333\";s:3:\"tel\";s:2:\"33\";s:3:\"fax\";s:2:\"33\";s:6:\"mobile\";s:2:\"33\";s:4:\"site\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(143,	1,	'新增账务',	1,	1611983172,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/finance/add',	'a:7:{s:4:\"type\";s:1:\"0\";s:4:\"c_id\";s:1:\"1\";s:7:\"bank_id\";s:1:\"1\";s:5:\"money\";s:1:\"1\";s:8:\"datetime\";s:0:\"\";s:7:\"attn_id\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(144,	1,	'撤销账单',	1,	1611983178,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/finance/query_delete?id=2',	'a:0:{}',	''),
(145,	1,	'新增财务分类',	1,	1611983197,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/finance/category_add',	'a:3:{s:4:\"type\";s:1:\"1\";s:3:\"pid\";s:0:\"\";s:4:\"name\";s:4:\"xxxx\";}',	''),
(146,	1,	'新增账务',	1,	1611983208,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/finance/add',	'a:7:{s:4:\"type\";s:1:\"1\";s:4:\"c_id\";s:1:\"3\";s:7:\"bank_id\";s:1:\"1\";s:5:\"money\";s:5:\"11111\";s:8:\"datetime\";s:16:\"2021-01-30 13:06\";s:7:\"attn_id\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(147,	1,	'修改银行',	1,	1611983222,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/finance/bank_edit',	'a:5:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:4:\"1111\";s:5:\"money\";s:5:\"12111\";s:6:\"remark\";s:5:\"11111\";s:7:\"default\";a:1:{i:0;s:1:\"1\";}}',	''),
(148,	1,	'打印出货单=>20210130311219005341',	1,	1611984013,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_view?id=14&session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.3190641027660437',	'a:0:{}',	''),
(149,	1,	'打印出货单=>20210130311219005341',	1,	1611984152,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_view?id=14&session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9803392790328203',	'a:0:{}',	''),
(150,	1,	'打印出货单',	1,	1611984391,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9467174804320821&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=',	'a:0:{}',	''),
(151,	1,	'打印出货单',	1,	1611984405,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9467174804320821&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=',	'a:0:{}',	''),
(152,	1,	'打印出货单',	1,	1611984407,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9467174804320821&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=',	'a:0:{}',	''),
(153,	1,	'打印出货单',	1,	1611984407,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9467174804320821&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=',	'a:0:{}',	''),
(154,	1,	'打印出货单',	1,	1611984413,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9467174804320821&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=',	'a:0:{}',	''),
(155,	1,	'打印出货单',	1,	1611984413,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9467174804320821&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=',	'a:0:{}',	''),
(156,	1,	'打印出货单',	1,	1611984413,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9467174804320821&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=',	'a:0:{}',	''),
(157,	1,	'打印出货单',	1,	1611984414,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9467174804320821&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=',	'a:0:{}',	''),
(158,	1,	'打印出货单',	1,	1611984414,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9467174804320821&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=',	'a:0:{}',	''),
(159,	1,	'打印出货单',	1,	1611984418,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.9467174804320821&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=',	'a:0:{}',	''),
(160,	1,	'打印入库单',	1,	1611984426,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/storage_view?id=11&session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.7381268731831647',	'a:0:{}',	''),
(161,	1,	'打印入库单',	1,	1611984430,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/storage_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.1577601799329642&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&warehouse=&supplier=&c_id=&type=',	'a:0:{}',	''),
(162,	1,	'打印出货单=>20210130311219005341',	1,	1611984445,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_view?id=14&session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.6752345470515653',	'a:0:{}',	''),
(163,	1,	'库存调拨',	1,	1611984454,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/transfer_add?id=5',	'a:3:{s:9:\"warehouse\";s:1:\"2\";s:6:\"number\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(164,	1,	'新增产品',	1,	1611985649,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/5fd32c46276a4.jpg\";s:4:\"name\";s:3:\"xxx\";s:4:\"code\";s:2:\"xx\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"1\";s:8:\"purchase\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(165,	1,	'新增产品',	1,	1611985659,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"3\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:2:\"ee\";s:4:\"code\";s:2:\"ee\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"1\";s:8:\"purchase\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(166,	1,	'新增产品',	1,	1611985673,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:3:\"eee\";s:4:\"code\";s:4:\"ee33\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"33\";s:8:\"purchase\";s:1:\"3\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(167,	1,	'新增产品',	1,	1611985685,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"2\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:3:\"333\";s:4:\"code\";s:4:\"ssss\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"1\";s:8:\"purchase\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(168,	1,	'新增产品',	1,	1611985705,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"3\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:2:\"33\";s:4:\"code\";s:4:\"gasd\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"2\";s:8:\"purchase\";s:1:\"2\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(169,	1,	'新增产品',	1,	1611985715,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"3\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:3:\"xxx\";s:4:\"code\";s:3:\"xxx\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"2\";s:8:\"purchase\";s:1:\"2\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(170,	1,	'更新产品',	1,	1611985903,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_edit',	'a:14:{s:2:\"id\";s:1:\"5\";s:12:\"http_referer\";s:70:\"http://127.0.0.1/_mmno_/mmno_ims/public/admin/configure/product?page=2\";s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:8:\"包材13\";s:4:\"code\";s:4:\"1111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"11\";s:8:\"purchase\";s:2:\"22\";s:4:\"type\";s:1:\"2\";s:6:\"remark\";s:0:\"\";}',	''),
(171,	1,	'更新产品',	1,	1611985949,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_edit',	'a:14:{s:2:\"id\";s:1:\"5\";s:12:\"http_referer\";s:70:\"http://127.0.0.1/_mmno_/mmno_ims/public/admin/configure/product?page=2\";s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:9:\"包材13x\";s:4:\"code\";s:4:\"1111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"11\";s:8:\"purchase\";s:2:\"22\";s:4:\"type\";s:1:\"2\";s:6:\"remark\";s:0:\"\";}',	''),
(172,	1,	'新增产品',	1,	1611986323,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_add',	'a:12:{s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/5fd32c46276a4.jpg\";s:4:\"name\";s:2:\"ee\";s:4:\"code\";s:5:\"ee111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"1\";s:8:\"purchase\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(173,	1,	'更新产品',	1,	1611986433,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_edit',	'a:13:{s:2:\"id\";s:1:\"5\";s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:10:\"包材13xx\";s:4:\"code\";s:4:\"1111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"11\";s:8:\"purchase\";s:2:\"22\";s:4:\"type\";s:1:\"2\";s:6:\"remark\";s:0:\"\";}',	''),
(174,	1,	'更新产品',	1,	1611986458,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_edit',	'a:13:{s:2:\"id\";s:1:\"5\";s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:11:\"包材13xxe\";s:4:\"code\";s:4:\"1111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"11\";s:8:\"purchase\";s:2:\"22\";s:4:\"type\";s:1:\"2\";s:6:\"remark\";s:0:\"\";}',	''),
(175,	1,	'更新产品',	1,	1611986631,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_edit',	'a:13:{s:2:\"id\";s:2:\"16\";s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/5fd32c46276a4.jpg\";s:4:\"name\";s:2:\"ee\";s:4:\"code\";s:5:\"ee111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"1\";s:8:\"purchase\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(176,	1,	'更新产品',	1,	1611986642,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_edit',	'a:13:{s:2:\"id\";s:2:\"16\";s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/5fd32c46276a4.jpg\";s:4:\"name\";s:2:\"ee\";s:4:\"code\";s:5:\"ee111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"1\";s:8:\"purchase\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(177,	1,	'更新产品',	1,	1611986644,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_edit',	'a:13:{s:2:\"id\";s:2:\"16\";s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/5fd32c46276a4.jpg\";s:4:\"name\";s:2:\"ee\";s:4:\"code\";s:5:\"ee111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"1\";s:8:\"purchase\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(178,	1,	'更新产品',	1,	1611986694,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_edit',	'a:13:{s:2:\"id\";s:2:\"16\";s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/5fd32c46276a4.jpg\";s:4:\"name\";s:2:\"ee\";s:4:\"code\";s:5:\"ee111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:1:\"1\";s:8:\"purchase\";s:1:\"1\";s:4:\"type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(179,	1,	'更新产品',	1,	1611986700,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/product_edit',	'a:13:{s:2:\"id\";s:1:\"5\";s:4:\"c_id\";s:1:\"1\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:4:\"name\";s:11:\"包材13xxe\";s:4:\"code\";s:4:\"1111\";s:6:\"format\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:6:\"lowest\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:5:\"sales\";s:2:\"11\";s:8:\"purchase\";s:2:\"22\";s:4:\"type\";s:1:\"2\";s:6:\"remark\";s:0:\"\";}',	''),
(180,	1,	'打印出货单=>20210130311219005341',	1,	1611986760,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_view?id=14&session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.2959974123826208',	'a:0:{}',	''),
(181,	1,	'打印出货单',	1,	1611988087,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/prints/orders_list?session_id=upn3mo6qs6m04h44ts917fnsn1&r=0.7097307482127666&chart=1&timea=2020-12-31&timeb=2021-01-30&keyword=&nickname=&tel=&sales_type=&warehouse=&c_id=&type=&status=&page_size=10',	'a:0:{}',	''),
(182,	1,	'成功登录系统',	1,	1611988429,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/everyone/login',	'a:2:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"111111\";}',	''),
(183,	31,	'成功登录系统',	1,	1611988881,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/everyone/login',	'a:2:{s:8:\"username\";s:4:\"test\";s:8:\"password\";s:6:\"111111\";}',	''),
(184,	31,	'修改仓库',	1,	1611988916,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/warehouse_edit',	'a:5:{s:2:\"id\";s:1:\"3\";s:4:\"name\";s:12:\"退货仓库\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:4:\"uids\";a:2:{i:0;s:1:\"1\";i:1;s:2:\"31\";}}',	''),
(185,	31,	'修改仓库',	1,	1611988919,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/warehouse_edit',	'a:6:{s:2:\"id\";s:1:\"2\";s:4:\"name\";s:12:\"上海仓库\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:7:\"default\";a:1:{i:0;s:1:\"1\";}s:4:\"uids\";a:2:{i:0;s:1:\"1\";i:1;s:2:\"31\";}}',	''),
(186,	31,	'修改仓库',	1,	1611988922,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/configure/warehouse_edit',	'a:6:{s:2:\"id\";s:1:\"1\";s:4:\"name\";s:12:\"默认仓库\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:7:\"default\";a:1:{i:0;s:1:\"1\";}s:4:\"uids\";a:2:{i:0;s:1:\"1\";i:1;s:2:\"31\";}}',	''),
(187,	1,	'成功登录系统',	1,	1611988930,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/everyone/login',	'a:2:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"111111\";}',	''),
(188,	31,	'成功登录系统',	1,	1611989191,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/everyone/login',	'a:2:{s:8:\"username\";s:4:\"test\";s:8:\"password\";s:6:\"111111\";}',	''),
(189,	31,	'新增会员',	1,	1611990539,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/member/add',	'a:11:{s:4:\"g_id\";s:0:\"\";s:8:\"nickname\";s:4:\"xxxx\";s:3:\"sex\";s:1:\"1\";s:4:\"card\";s:0:\"\";s:3:\"tel\";s:0:\"\";s:2:\"qq\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"address\";s:0:\"\";s:7:\"id_card\";s:0:\"\";s:8:\"birthday\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(190,	31,	'新增会员',	1,	1611990572,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/member/add',	'a:11:{s:4:\"g_id\";s:0:\"\";s:8:\"nickname\";s:3:\"vvv\";s:3:\"sex\";s:1:\"1\";s:4:\"card\";s:0:\"\";s:3:\"tel\";s:0:\"\";s:2:\"qq\";s:0:\"\";s:5:\"email\";s:0:\"\";s:7:\"address\";s:0:\"\";s:7:\"id_card\";s:0:\"\";s:8:\"birthday\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(191,	31,	'销售产品',	1,	1611990697,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:3:\"111\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:7:\"1221.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(192,	31,	'销售产品',	1,	1611990706,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(193,	31,	'销售产品',	1,	1611990706,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(194,	31,	'销售产品',	1,	1611990706,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(195,	31,	'销售产品',	1,	1611990706,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(196,	31,	'销售产品',	1,	1611990706,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(197,	31,	'销售产品',	1,	1611990707,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(198,	31,	'销售产品',	1,	1611990707,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(199,	31,	'销售产品',	1,	1611990707,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(200,	31,	'销售产品',	1,	1611990707,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(201,	31,	'销售产品',	1,	1611990707,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(202,	31,	'销售产品',	1,	1611990707,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:2:\"11\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:6:\"121.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(203,	31,	'销售产品',	1,	1611990723,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(204,	31,	'销售产品',	1,	1611990723,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(205,	31,	'销售产品',	1,	1611990723,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(206,	31,	'销售产品',	1,	1611990723,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(207,	31,	'销售产品',	1,	1611990723,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(208,	31,	'销售产品',	1,	1611990723,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(209,	31,	'销售产品',	1,	1611990724,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(210,	31,	'销售产品',	1,	1611990724,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(211,	31,	'销售产品',	1,	1611990724,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(212,	31,	'销售产品',	1,	1611990724,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(213,	31,	'销售产品',	1,	1611990724,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(214,	31,	'销售产品',	1,	1611990724,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:11\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(215,	31,	'销售产品',	1,	1611991123,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(216,	31,	'销售产品',	1,	1611991124,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(217,	31,	'销售产品',	1,	1611991124,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(218,	31,	'销售产品',	1,	1611991124,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(219,	31,	'销售产品',	1,	1611991124,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(220,	31,	'销售产品',	1,	1611991124,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(221,	31,	'销售产品',	1,	1611991124,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(222,	31,	'销售产品',	1,	1611991125,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(223,	31,	'销售产品',	1,	1611991125,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(224,	31,	'销售产品',	1,	1611991125,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(225,	31,	'销售产品',	1,	1611991125,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:18\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(226,	31,	'销售产品',	1,	1611991400,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"1\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"11.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:23\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:0:\"\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(227,	31,	'更新会员',	1,	1611991535,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/member/edit',	'a:12:{s:2:\"id\";s:1:\"5\";s:4:\"g_id\";s:0:\"\";s:8:\"nickname\";s:3:\"vvv\";s:3:\"sex\";s:1:\"1\";s:4:\"card\";s:16:\"1111222233334444\";s:3:\"tel\";s:11:\"18911112222\";s:2:\"qq\";s:9:\"110111222\";s:5:\"email\";s:14:\"bjphper@qq.com\";s:7:\"address\";s:12:\"北京西城\";s:7:\"id_card\";s:14:\"33098776554334\";s:8:\"birthday\";s:0:\"\";s:6:\"remark\";s:0:\"\";}',	''),
(228,	31,	'更新会员',	1,	1611991586,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/member/edit',	'a:12:{s:2:\"id\";s:1:\"5\";s:4:\"g_id\";s:0:\"\";s:8:\"nickname\";s:3:\"vvv\";s:3:\"sex\";s:1:\"1\";s:4:\"card\";s:16:\"1111222233334444\";s:3:\"tel\";s:11:\"18911112222\";s:2:\"qq\";s:9:\"110111222\";s:5:\"email\";s:14:\"bjphper@qq.com\";s:7:\"address\";s:12:\"北京西城\";s:7:\"id_card\";s:14:\"33098776554334\";s:8:\"birthday\";s:10:\"2012-01-30\";s:6:\"remark\";s:0:\"\";}',	''),
(229,	31,	'更新会员',	1,	1611991599,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/member/edit',	'a:12:{s:2:\"id\";s:1:\"5\";s:4:\"g_id\";s:0:\"\";s:8:\"nickname\";s:3:\"vvv\";s:3:\"sex\";s:1:\"1\";s:4:\"card\";s:16:\"1111222233334444\";s:3:\"tel\";s:11:\"18911112222\";s:2:\"qq\";s:9:\"110111222\";s:5:\"email\";s:14:\"bjphper@qq.com\";s:7:\"address\";s:12:\"北京西城\";s:7:\"id_card\";s:14:\"33098776554334\";s:8:\"birthday\";s:10:\"2012-01-30\";s:6:\"remark\";s:9:\"主要VIP\";}',	''),
(230,	31,	'销售产品',	1,	1611991689,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"2\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"22.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:24\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:12:\"北京西城\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(231,	31,	'销售产品',	1,	1611991689,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"2\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"22.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:24\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:12:\"北京西城\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(232,	31,	'销售产品',	1,	1611991689,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"2\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"22.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:24\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:12:\"北京西城\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(233,	31,	'销售产品',	1,	1611991690,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"2\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"22.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:24\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:12:\"北京西城\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(234,	31,	'销售产品',	1,	1611991690,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"2\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"22.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:24\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:12:\"北京西城\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(235,	31,	'销售产品',	1,	1611991690,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"2\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"22.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:24\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:12:\"北京西城\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(236,	31,	'销售产品',	1,	1611991691,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/inventory/sales_submit',	'a:13:{s:9:\"member_id\";s:1:\"5\";s:10:\"product_id\";s:0:\"\";s:11:\"product_ids\";a:1:{i:4;s:1:\"4\";}s:16:\"product_quantity\";a:1:{i:4;s:1:\"2\";}s:11:\"group_price\";a:1:{i:4;s:2:\"11\";}s:17:\"product_warehouse\";a:1:{i:4;s:1:\"2\";}s:6:\"amount\";s:5:\"22.00\";s:9:\"ship_time\";s:16:\"2021-01-30 15:24\";s:10:\"express_id\";s:0:\"\";s:11:\"express_num\";s:0:\"\";s:12:\"express_addr\";s:12:\"北京西城\";s:10:\"sales_type\";s:1:\"1\";s:6:\"remark\";s:0:\"\";}',	''),
(237,	31,	'更新会员',	1,	1611991713,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/member/edit',	'a:12:{s:2:\"id\";s:1:\"5\";s:4:\"g_id\";s:1:\"1\";s:8:\"nickname\";s:3:\"vvv\";s:3:\"sex\";s:1:\"1\";s:4:\"card\";s:16:\"1111222233334444\";s:3:\"tel\";s:11:\"18911112222\";s:2:\"qq\";s:9:\"110111222\";s:5:\"email\";s:14:\"bjphper@qq.com\";s:7:\"address\";s:12:\"北京西城\";s:7:\"id_card\";s:14:\"33098776554334\";s:8:\"birthday\";s:10:\"2012-01-30\";s:6:\"remark\";s:9:\"主要VIP\";}',	''),
(238,	1,	'成功登录系统',	1,	1611991780,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/mmno_ims/public/admin/everyone/login',	'a:2:{s:8:\"username\";s:10:\"superadmin\";s:8:\"password\";s:6:\"111111\";}',	'');

DROP TABLE IF EXISTS `tb_operate_3`;
CREATE TABLE `tb_operate_3` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';


DROP TABLE IF EXISTS `tb_operate_4`;
CREATE TABLE `tb_operate_4` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';


DROP TABLE IF EXISTS `tb_operate_5`;
CREATE TABLE `tb_operate_5` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';


DROP TABLE IF EXISTS `tb_operate_6`;
CREATE TABLE `tb_operate_6` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';


DROP TABLE IF EXISTS `tb_operate_7`;
CREATE TABLE `tb_operate_7` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';

INSERT INTO `tb_operate_7` (`id`, `u_id`, `title`, `status`, `create_time`, `ip`, `client`, `country`, `area`, `url`, `data`, `log`) VALUES
(1,	26,	'成功登录系统',	1,	1558939985,	'127.0.0.1',	'pc',	'',	'',	'/_mmno_/pss.mmno.com/public/admin/everyone/login',	'a:3:{s:8:\"username\";s:2:\"xx\";s:8:\"password\";s:6:\"123456\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(2,	16,	'成功登录系统',	1,	1558946030,	'223.72.67.182',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:7:\"zhaodan\";s:8:\"password\";s:6:\"111111\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(3,	16,	'用户密码错误',	0,	1558946559,	'114.253.225.248',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:7:\"zhaodan\";s:8:\"password\";s:7:\"narrims\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(4,	16,	'成功登录系统',	1,	1558946562,	'114.253.225.248',	'pc',	'',	'',	'/admin/everyone/login',	'a:3:{s:8:\"username\";s:7:\"zhaodan\";s:8:\"password\";s:6:\"111111\";s:15:\"rember_password\";s:2:\"on\";}',	''),
(5,	16,	'新增仓库',	0,	1558947225,	'114.253.225.248',	'pc',	'',	'',	'/admin/configure/warehouse_add',	'a:4:{s:4:\"name\";s:9:\"北京仓\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:4:\"uids\";a:5:{i:0;s:1:\"1\";i:1;s:2:\"16\";i:2;s:2:\"17\";i:3;s:2:\"18\";i:4;s:2:\"27\";}}',	''),
(6,	16,	'新增仓库',	1,	1558947430,	'114.253.225.248',	'pc',	'',	'',	'/admin/configure/warehouse_add',	'a:4:{s:4:\"name\";s:9:\"北京仓\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:4:\"uids\";a:5:{i:0;s:1:\"1\";i:1;s:2:\"16\";i:2;s:2:\"17\";i:3;s:2:\"18\";i:4;s:2:\"27\";}}',	''),
(7,	16,	'新增仓库',	1,	1558947443,	'114.253.225.248',	'pc',	'',	'',	'/admin/configure/warehouse_add',	'a:4:{s:4:\"name\";s:9:\"宁波仓\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:4:\"uids\";a:5:{i:0;s:1:\"1\";i:1;s:2:\"16\";i:2;s:2:\"17\";i:3;s:2:\"18\";i:4;s:2:\"27\";}}',	''),
(8,	16,	'新增仓库',	1,	1558947454,	'114.253.225.248',	'pc',	'',	'',	'/admin/configure/warehouse_add',	'a:4:{s:4:\"name\";s:9:\"广州仓\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:4:\"uids\";a:5:{i:0;s:1:\"1\";i:1;s:2:\"16\";i:2;s:2:\"17\";i:3;s:2:\"18\";i:4;s:2:\"27\";}}',	''),
(9,	16,	'新增仓库',	1,	1558947465,	'114.253.225.248',	'pc',	'',	'',	'/admin/configure/warehouse_add',	'a:4:{s:4:\"name\";s:9:\"郑州仓\";s:7:\"address\";s:0:\"\";s:6:\"remark\";s:0:\"\";s:4:\"uids\";a:6:{i:0;s:1:\"1\";i:1;s:2:\"16\";i:2;s:2:\"17\";i:3;s:2:\"18\";i:4;s:2:\"19\";i:5;s:2:\"27\";}}',	'');

DROP TABLE IF EXISTS `tb_operate_8`;
CREATE TABLE `tb_operate_8` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';


DROP TABLE IF EXISTS `tb_operate_9`;
CREATE TABLE `tb_operate_9` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `title` varchar(255) DEFAULT NULL,
  `status` int(1) DEFAULT '1',
  `create_time` int(11) DEFAULT NULL,
  `ip` varchar(255) DEFAULT NULL,
  `client` varchar(15) DEFAULT 'pc' COMMENT '客户端',
  `country` varchar(255) DEFAULT NULL,
  `area` varchar(255) DEFAULT NULL,
  `url` varchar(255) DEFAULT NULL,
  `data` text,
  `log` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COMMENT='操作日志';


DROP TABLE IF EXISTS `tb_pinyin`;
CREATE TABLE `tb_pinyin` (
  `py` char(1) NOT NULL,
  `begin` smallint(5) unsigned NOT NULL,
  `end` smallint(5) unsigned NOT NULL,
  PRIMARY KEY (`py`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tb_pinyin` (`py`, `begin`, `end`) VALUES
('A',	45217,	45252),
('B',	45253,	45760),
('C',	45761,	46317),
('D',	46318,	46825),
('E',	46826,	47009),
('F',	47010,	47296),
('G',	47297,	47613),
('H',	47614,	48118),
('J',	48119,	49061),
('K',	49062,	49323),
('L',	49324,	49895),
('M',	49896,	50370),
('N',	50371,	50613),
('O',	50614,	50621),
('P',	50622,	50905),
('Q',	50906,	51386),
('R',	51387,	51445),
('S',	51446,	52217),
('T',	52218,	52697),
('W',	52698,	52979),
('X',	52980,	53640),
('Y',	53689,	54480),
('Z',	54481,	55289);

DROP TABLE IF EXISTS `tb_product`;
CREATE TABLE `tb_product` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL COMMENT '创建员工',
  `c_id` int(11) DEFAULT NULL COMMENT '产品分类',
  `code` varchar(255) DEFAULT NULL COMMENT '产品货号',
  `name` varchar(255) DEFAULT NULL COMMENT '产品名称',
  `image` varchar(255) NOT NULL COMMENT '产品主图',
  `sales` double(11,2) DEFAULT NULL COMMENT '销售价',
  `purchase` double(11,2) DEFAULT NULL COMMENT '进货价',
  `points` bigint(20) DEFAULT '0' COMMENT '积分',
  `format` varchar(255) DEFAULT NULL COMMENT '产品规格',
  `lowest` int(11) DEFAULT '0' COMMENT '最低库存',
  `type` int(1) DEFAULT '0' COMMENT '产品类型',
  `unit` varchar(255) DEFAULT NULL COMMENT '单位',
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `update_uid` int(11) DEFAULT NULL,
  `remark` text COMMENT '备注',
  `bar_code` varchar(100) DEFAULT NULL COMMENT '条码',
  PRIMARY KEY (`id`),
  KEY `uid` (`u_id`),
  KEY `c_id` (`c_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品';

INSERT INTO `tb_product` (`id`, `u_id`, `c_id`, `code`, `name`, `image`, `sales`, `purchase`, `points`, `format`, `lowest`, `type`, `unit`, `create_time`, `update_time`, `update_uid`, `remark`, `bar_code`) VALUES
(4,	1,	1,	'111',	'牙刷',	'image/6014242b29a5f.jpg',	11.00,	22.00,	0,	'',	0,	1,	'',	1611932729,	1611933130,	1,	'',	''),
(5,	1,	1,	'1111',	'包材13xxe',	'image/6014262fd6340.jpg',	11.00,	22.00,	0,	'',	0,	2,	'',	1611933266,	1611986700,	1,	'',	''),
(6,	1,	2,	'11',	'11',	'image/6014de5900a5f.jpg',	11.00,	11.00,	0,	'',	0,	1,	'',	1611980399,	1611980399,	1,	'',	''),
(7,	1,	2,	'1122',	'xxx',	'image/6014262fd6340.jpg',	1.00,	1.00,	0,	'',	0,	1,	'',	1611980417,	1611980417,	1,	'',	''),
(8,	1,	2,	'22',	'22',	'image/6014262fd6340.jpg',	2.00,	2.00,	0,	'',	0,	1,	'',	1611980451,	1611980451,	1,	'',	''),
(9,	1,	1,	'33',	'33',	'image/5fd32c46276a4.jpg',	33.00,	33.00,	0,	'',	0,	1,	'',	1611980463,	1611980463,	1,	'',	''),
(10,	1,	1,	'xx',	'xxx',	'image/5fd32c46276a4.jpg',	1.00,	1.00,	0,	'',	0,	1,	'',	1611985649,	1611985649,	1,	'',	''),
(11,	1,	3,	'ee',	'ee',	'image/6014262fd6340.jpg',	1.00,	1.00,	0,	'',	0,	1,	'',	1611985659,	1611985659,	1,	'',	''),
(12,	1,	1,	'ee33',	'eee',	'image/6014262fd6340.jpg',	33.00,	3.00,	0,	'',	0,	1,	'',	1611985673,	1611985673,	1,	'',	''),
(13,	1,	2,	'ssss',	'333',	'image/6014262fd6340.jpg',	1.00,	1.00,	0,	'',	0,	1,	'',	1611985685,	1611985685,	1,	'',	''),
(14,	1,	3,	'gasd',	'33',	'image/6014262fd6340.jpg',	2.00,	2.00,	0,	'',	0,	1,	'',	1611985705,	1611985705,	1,	'',	''),
(15,	1,	3,	'xxx',	'xxx',	'image/6014262fd6340.jpg',	2.00,	2.00,	0,	'',	0,	1,	'',	1611985715,	1611985715,	1,	'',	''),
(16,	1,	1,	'ee111',	'ee',	'image/5fd32c46276a4.jpg',	1.00,	1.00,	0,	'',	0,	1,	'',	1611986323,	1611986694,	1,	'',	'');

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `tb_product_build_order`;
CREATE TABLE `tb_product_build_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(50) NOT NULL COMMENT '生产订单号',
  `u_id` int(11) NOT NULL,
  `p_id` int(11) NOT NULL COMMENT '生产的产品',
  `quantity` int(11) NOT NULL COMMENT '生产数量 ',
  `build_time` int(11) NOT NULL COMMENT '生产日期',
  `remark` varchar(50) NOT NULL COMMENT '备注',
  `create_time` int(11) NOT NULL COMMENT '创建日期',
  `storage_order_id` int(11) NOT NULL COMMENT '关联的入库订单',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tb_product_build_order` (`id`, `order_number`, `u_id`, `p_id`, `quantity`, `build_time`, `remark`, `create_time`, `storage_order_id`) VALUES
(1,	'20210130311106067371',	1,	4,	5,	1611975900,	'',	1611975966,	11);

DROP TABLE IF EXISTS `tb_product_build_order_data`;
CREATE TABLE `tb_product_build_order_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `o_id` int(11) NOT NULL COMMENT '订单',
  `p_id_bc` int(11) NOT NULL COMMENT '包材名称',
  `w_id` int(11) NOT NULL COMMENT '来自哪个仓库',
  `product_data` text NOT NULL COMMENT '产品快照',
  `quantity` int(11) NOT NULL COMMENT '消耗数量 ',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tb_product_build_order_data` (`id`, `o_id`, `p_id_bc`, `w_id`, `product_data`, `quantity`) VALUES
(1,	1,	5,	2,	'a:19:{s:2:\"id\";i:5;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:4:\"1111\";s:4:\"name\";s:7:\"包材1\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";s:0:\"\";s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611933266;s:11:\"update_time\";i:1611933266;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";}',	50);

DROP TABLE IF EXISTS `tb_product_category`;
CREATE TABLE `tb_product_category` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL,
  `pid` int(11) DEFAULT '0',
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产器分类';

INSERT INTO `tb_product_category` (`id`, `name`, `pid`, `sort`) VALUES
(1,	'水果',	2,	0),
(2,	'干货',	0,	0),
(3,	'美妆',	0,	0),
(4,	'eee',	0,	0);

DROP TABLE IF EXISTS `tb_product_inventory`;
CREATE TABLE `tb_product_inventory` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` varchar(255) DEFAULT NULL COMMENT '产品',
  `w_id` varchar(255) DEFAULT NULL COMMENT '仓库',
  `quantity` bigint(255) DEFAULT NULL COMMENT '数量',
  PRIMARY KEY (`id`),
  KEY `product` (`p_id`),
  KEY `warehouse` (`w_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='库存表';

INSERT INTO `tb_product_inventory` (`id`, `p_id`, `w_id`, `quantity`) VALUES
(4,	'4',	'2',	5721),
(5,	'4',	'1',	0),
(6,	'5',	'2',	11060),
(7,	'4',	'3',	5);

DROP TABLE IF EXISTS `tb_product_relation`;
CREATE TABLE `tb_product_relation` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `p_id` int(11) NOT NULL,
  `p_id_bc` int(11) NOT NULL,
  `multiple` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tb_product_relation` (`id`, `p_id`, `p_id_bc`, `multiple`) VALUES
(1,	4,	5,	10);

DROP TABLE IF EXISTS `tb_product_sales_order`;
CREATE TABLE `tb_product_sales_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(100) DEFAULT NULL COMMENT '订单号',
  `status` int(2) DEFAULT '1' COMMENT '状态',
  `u_id` int(11) DEFAULT NULL COMMENT '创建员工',
  `m_id` int(11) DEFAULT NULL COMMENT '购买会员',
  `create_time` int(11) DEFAULT NULL,
  `remark` text,
  `ship_time` int(11) DEFAULT NULL COMMENT '发货日期',
  `print` int(11) DEFAULT '0' COMMENT '是否打印',
  `amount` double(22,2) DEFAULT NULL COMMENT '金额',
  `points` double(22,2) DEFAULT NULL COMMENT '积分',
  `cost` double(20,2) DEFAULT NULL COMMENT '销售成本',
  `express_id` int(11) DEFAULT NULL COMMENT '快递',
  `express_num` varchar(50) DEFAULT NULL,
  `express_addr` varchar(500) DEFAULT NULL,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单管理';

INSERT INTO `tb_product_sales_order` (`id`, `order_number`, `status`, `u_id`, `m_id`, `create_time`, `remark`, `ship_time`, `print`, `amount`, `points`, `cost`, `express_id`, `express_num`, `express_addr`, `type`) VALUES
(1,	'20190528311257263831',	1,	1,	2,	1559019446,	'',	1559019420,	1,	400.00,	NULL,	200.00,	0,	'',	'地址地址地址地址地址地址地址',	1),
(2,	'20190529311030545051',	1,	1,	0,	1559097054,	'123',	1559097000,	0,	1.00,	NULL,	1.00,	1,	'',	'21132',	1),
(3,	'20190529311031521201',	1,	1,	0,	1559097112,	'',	1559097060,	0,	4.00,	NULL,	4.00,	0,	'',	'',	1),
(4,	'20210129312320073741',	1,	1,	0,	1611933607,	'',	1611933540,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(5,	'20210130311218173461',	1,	1,	0,	1611980297,	'',	1611980280,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(6,	'20210130311218214691',	1,	1,	0,	1611980301,	'',	1611980280,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(7,	'20210130311218268291',	1,	1,	0,	1611980306,	'',	1611980280,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(8,	'20210130311218308081',	1,	1,	0,	1611980310,	'',	1611980280,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(9,	'20210130311218352131',	1,	1,	0,	1611980315,	'',	1611980280,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(10,	'20210130311218396951',	1,	1,	0,	1611980319,	'',	1611980280,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(11,	'20210130311218432691',	1,	1,	0,	1611980323,	'',	1611980280,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(12,	'20210130311218484761',	1,	1,	0,	1611980328,	'',	1611980280,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(13,	'20210130311218548441',	1,	1,	0,	1611980334,	'',	1611980280,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(14,	'20210130311219005341',	1,	1,	0,	1611980340,	'',	1611980280,	1,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(15,	'202101303115113771031',	1,	31,	5,	1611990697,	'',	1611990660,	0,	1221.00,	NULL,	2442.00,	0,	'',	'',	1),
(16,	'202101303115114669731',	1,	31,	5,	1611990706,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(17,	'202101303115114666131',	1,	31,	5,	1611990706,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(18,	'202101303115114686331',	1,	31,	5,	1611990706,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(19,	'202101303115114666531',	1,	31,	5,	1611990706,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(20,	'202101303115114690831',	1,	31,	5,	1611990706,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(21,	'202101303115114753131',	1,	31,	5,	1611990707,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(22,	'202101303115114784631',	1,	31,	5,	1611990707,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(23,	'202101303115114729631',	1,	31,	5,	1611990707,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(24,	'202101303115114789731',	1,	31,	5,	1611990707,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(25,	'202101303115114766431',	1,	31,	5,	1611990707,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(26,	'202101303115114729931',	1,	31,	5,	1611990707,	'',	1611990660,	0,	121.00,	NULL,	242.00,	0,	'',	'',	1),
(27,	'202101303115120329731',	1,	31,	5,	1611990723,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(28,	'202101303115120335531',	1,	31,	5,	1611990723,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(29,	'202101303115120332731',	1,	31,	5,	1611990723,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(30,	'202101303115120367531',	1,	31,	5,	1611990723,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(31,	'202101303115120328031',	1,	31,	5,	1611990723,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(32,	'202101303115120374231',	1,	31,	5,	1611990723,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(33,	'202101303115120449731',	1,	31,	5,	1611990724,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(34,	'202101303115120423431',	1,	31,	5,	1611990724,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(35,	'202101303115120417631',	1,	31,	5,	1611990724,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(36,	'202101303115120412031',	1,	31,	5,	1611990724,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(37,	'202101303115120463331',	1,	31,	5,	1611990724,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(38,	'202101303115120441531',	1,	31,	5,	1611990724,	'',	1611990660,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(39,	'202101303115184339531',	1,	31,	5,	1611991123,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(40,	'202101303115184412831',	1,	31,	5,	1611991124,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(41,	'202101303115184487431',	1,	31,	5,	1611991124,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(42,	'202101303115184455331',	1,	31,	5,	1611991124,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(43,	'202101303115184484331',	1,	31,	5,	1611991124,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(44,	'202101303115184425731',	1,	31,	5,	1611991124,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(45,	'202101303115184487931',	1,	31,	5,	1611991124,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(46,	'202101303115184536431',	1,	31,	5,	1611991125,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(47,	'202101303115184542031',	1,	31,	5,	1611991125,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(48,	'202101303115184563731',	1,	31,	5,	1611991125,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(49,	'202101303115184545731',	1,	31,	5,	1611991125,	'',	1611991080,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(50,	'202101303115232094931',	1,	31,	5,	1611991400,	'',	1611991380,	0,	11.00,	NULL,	22.00,	0,	'',	'',	1),
(51,	'202101303115280952831',	1,	31,	5,	1611991689,	'',	1611991440,	0,	22.00,	NULL,	44.00,	0,	'',	'北京西城',	1),
(52,	'202101303115280936031',	1,	31,	5,	1611991689,	'',	1611991440,	0,	22.00,	NULL,	44.00,	0,	'',	'北京西城',	1),
(53,	'202101303115280963931',	1,	31,	5,	1611991689,	'',	1611991440,	0,	22.00,	NULL,	44.00,	0,	'',	'北京西城',	1),
(54,	'202101303115281023731',	1,	31,	5,	1611991690,	'',	1611991440,	0,	22.00,	NULL,	44.00,	0,	'',	'北京西城',	1),
(55,	'202101303115281080131',	1,	31,	5,	1611991690,	'',	1611991440,	0,	22.00,	NULL,	44.00,	0,	'',	'北京西城',	1),
(56,	'202101303115281071431',	1,	31,	5,	1611991690,	'',	1611991440,	0,	22.00,	NULL,	44.00,	0,	'',	'北京西城',	1),
(57,	'202101303115281087631',	1,	31,	5,	1611991690,	'',	1611991440,	0,	22.00,	NULL,	44.00,	0,	'',	'北京西城',	1);

DROP TABLE IF EXISTS `tb_product_sales_order_data`;
CREATE TABLE `tb_product_sales_order_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `o_id` int(11) DEFAULT NULL COMMENT '订单ID',
  `status` int(255) DEFAULT '1' COMMENT '状态',
  `p_id` int(11) DEFAULT NULL COMMENT '产品',
  `quantity` bigint(20) DEFAULT NULL COMMENT '数量',
  `discounts` decimal(4,2) DEFAULT NULL COMMENT '折扣',
  `amount` double(20,2) DEFAULT '0.00' COMMENT '金额',
  `cost` double(20,2) DEFAULT NULL,
  `points` bigint(20) DEFAULT '0' COMMENT '购买积分',
  `w_id` int(11) DEFAULT NULL COMMENT '仓库',
  `product_data` text COMMENT '产品数据',
  `returns` bigint(20) DEFAULT '0' COMMENT '退货数量',
  `group_price` double(20,2) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `o_id` (`o_id`),
  KEY `warehouse` (`w_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='订单数据';

INSERT INTO `tb_product_sales_order_data` (`id`, `o_id`, `status`, `p_id`, `quantity`, `discounts`, `amount`, `cost`, `points`, `w_id`, `product_data`, `returns`, `group_price`) VALUES
(1,	1,	1,	3,	200,	NULL,	400.00,	200.00,	0,	2,	'a:18:{s:2:\"id\";i:3;s:4:\"u_id\";i:1;s:4:\"c_id\";i:3;s:4:\"code\";s:4:\"0008\";s:4:\"name\";s:8:\"MG面膜\";s:5:\"sales\";d:5;s:8:\"purchase\";d:1;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:1:\"4\";s:11:\"create_time\";i:1559019309;s:11:\"update_time\";i:1559019309;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"category\";s:6:\"美妆\";s:12:\"product_type\";s:6:\"正常\";}',	0,	2.00),
(2,	2,	1,	1,	1,	NULL,	1.00,	1.00,	0,	2,	'a:18:{s:2:\"id\";i:1;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:5:\"00001\";s:4:\"name\";s:6:\"苹果\";s:5:\"sales\";d:1;s:8:\"purchase\";d:1;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:1:\"1\";s:11:\"create_time\";i:1559019025;s:11:\"update_time\";i:1559019025;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"正常\";}',	0,	1.00),
(3,	3,	1,	1,	4,	NULL,	4.00,	4.00,	0,	2,	'a:18:{s:2:\"id\";i:1;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:5:\"00001\";s:4:\"name\";s:6:\"苹果\";s:5:\"sales\";d:1;s:8:\"purchase\";d:1;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:1:\"1\";s:11:\"create_time\";i:1559019025;s:11:\"update_time\";i:1559019025;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"正常\";}',	0,	1.00),
(4,	4,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0,	11.00),
(5,	5,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0,	11.00),
(6,	6,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0,	11.00),
(7,	7,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0,	11.00),
(8,	8,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0,	11.00),
(9,	9,	1,	5,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:5;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:4:\"1111\";s:4:\"name\";s:7:\"包材1\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:2;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611933266;s:11:\"update_time\";i:1611933266;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"赠品\";}',	0,	11.00),
(10,	10,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0,	11.00),
(11,	11,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0,	11.00),
(12,	12,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0,	11.00),
(13,	13,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0,	11.00),
(14,	14,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0,	11.00),
(15,	15,	1,	4,	111,	NULL,	1221.00,	2442.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(16,	16,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(17,	17,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(18,	18,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(19,	19,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(20,	20,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(21,	21,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(22,	22,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(23,	23,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(24,	24,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(25,	25,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(26,	26,	1,	4,	11,	NULL,	121.00,	242.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(27,	27,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(28,	28,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(29,	29,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(30,	30,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(31,	31,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(32,	32,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(33,	33,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(34,	34,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(35,	35,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(36,	36,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(37,	37,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(38,	38,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(39,	39,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(40,	40,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(41,	41,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(42,	42,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(43,	43,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(44,	44,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(45,	45,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(46,	46,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(47,	47,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(48,	48,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(49,	49,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(50,	50,	1,	4,	1,	NULL,	11.00,	22.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(51,	51,	1,	4,	2,	NULL,	22.00,	44.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(52,	52,	1,	4,	2,	NULL,	22.00,	44.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(53,	53,	1,	4,	2,	NULL,	22.00,	44.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(54,	54,	1,	4,	2,	NULL,	22.00,	44.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(55,	55,	1,	4,	2,	NULL,	22.00,	44.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(56,	56,	1,	4,	2,	NULL,	22.00,	44.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00),
(57,	57,	1,	4,	2,	NULL,	22.00,	44.00,	0,	2,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:7:\"成品5\";}',	0,	11.00);

DROP TABLE IF EXISTS `tb_product_sales_return`;
CREATE TABLE `tb_product_sales_return` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL COMMENT '员工',
  `o_id` int(11) DEFAULT NULL COMMENT '订单',
  `create_time` int(11) DEFAULT NULL COMMENT '时间',
  `quantity` bigint(20) DEFAULT NULL COMMENT '退货数量',
  `w_id` int(11) DEFAULT NULL COMMENT '退货仓库',
  `remark` text COMMENT '备注',
  PRIMARY KEY (`id`),
  KEY `uid` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品退货';


DROP TABLE IF EXISTS `tb_product_scrapped`;
CREATE TABLE `tb_product_scrapped` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL COMMENT '用户',
  `create_time` int(11) DEFAULT NULL,
  `remark` text COMMENT '备注',
  `p_id` bigint(20) DEFAULT NULL,
  `w_id` bigint(20) DEFAULT NULL,
  `quantity` bigint(20) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='报销产品';

INSERT INTO `tb_product_scrapped` (`id`, `u_id`, `create_time`, `remark`, `p_id`, `w_id`, `quantity`) VALUES
(1,	1,	1611975902,	'',	1,	1,	2),
(2,	1,	1611975916,	'',	1,	2,	2996);

DROP TABLE IF EXISTS `tb_product_storage_order`;
CREATE TABLE `tb_product_storage_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(255) DEFAULT NULL COMMENT '订单号',
  `u_id` int(11) DEFAULT NULL,
  `create_time` int(10) DEFAULT NULL,
  `s_id` int(11) DEFAULT NULL COMMENT '供应商',
  `quantity` bigint(20) DEFAULT NULL COMMENT '数量',
  `amount` double(20,2) DEFAULT NULL COMMENT '金额',
  `remark` text,
  `type` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`u_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tb_product_storage_order` (`id`, `order_number`, `u_id`, `create_time`, `s_id`, `quantity`, `amount`, `remark`, `type`) VALUES
(1,	'20190528311252414901',	1,	1559019161,	0,	1000,	1000.00,	'',	1),
(2,	'20190528311253599831',	1,	1559019239,	3,	2000,	4000.00,	'',	1),
(3,	'20190528311255464171',	1,	1559019346,	1,	200,	200.00,	'',	1),
(5,	'20190529311030099241',	1,	1559097009,	3,	2,	2.00,	'123',	1),
(6,	'20190529311031316481',	1,	1559097091,	0,	1,	1.00,	'',	1),
(7,	'20210129312316596831',	1,	1611933419,	1,	5000,	110.00,	'',	1),
(8,	'20210129312319566111',	1,	1611933596,	1,	1000,	22.00,	'',	1),
(9,	'20210130311054246361',	1,	1611975264,	0,	1,	0.00,	'',	1),
(10,	'20210130311105392711',	1,	1611975939,	0,	11111,	244442.00,	'',	1),
(11,	'20210130311106068561',	1,	1611975966,	0,	5,	0.00,	'生产自动入库',	3);

DROP TABLE IF EXISTS `tb_product_storage_order_data`;
CREATE TABLE `tb_product_storage_order_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `o_id` int(11) DEFAULT NULL,
  `w_id` int(11) DEFAULT '0' COMMENT '仓库',
  `s_id` int(11) DEFAULT NULL COMMENT '供应商',
  `p_id` int(11) DEFAULT '0' COMMENT '产品',
  `quantity` bigint(20) DEFAULT NULL COMMENT '数量',
  `create_time` int(10) DEFAULT NULL,
  `u_id` int(11) DEFAULT NULL,
  `remark` text COMMENT '备注',
  `returns` bigint(20) DEFAULT '0' COMMENT '退货',
  `product_data` text NOT NULL,
  `amount` double(20,2) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `o_id` (`o_id`),
  KEY `w_id` (`w_id`),
  KEY `s_id` (`s_id`),
  KEY `p_id` (`p_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='产品仓库';

INSERT INTO `tb_product_storage_order_data` (`id`, `o_id`, `w_id`, `s_id`, `p_id`, `quantity`, `create_time`, `u_id`, `remark`, `returns`, `product_data`, `amount`) VALUES
(1,	1,	2,	0,	1,	1000,	1559019161,	1,	NULL,	0,	'a:18:{s:2:\"id\";i:1;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:5:\"00001\";s:4:\"name\";s:6:\"苹果\";s:5:\"sales\";d:1;s:8:\"purchase\";d:1;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:1:\"1\";s:11:\"create_time\";i:1559019025;s:11:\"update_time\";i:1559019025;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"正常\";}',	1000.00),
(2,	2,	2,	3,	1,	2000,	1559019239,	1,	NULL,	0,	'a:18:{s:2:\"id\";i:1;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:5:\"00001\";s:4:\"name\";s:6:\"苹果\";s:5:\"sales\";d:1;s:8:\"purchase\";d:1;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:1:\"1\";s:11:\"create_time\";i:1559019025;s:11:\"update_time\";i:1559019025;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"正常\";}',	4000.00),
(3,	3,	2,	1,	3,	200,	1559019346,	1,	NULL,	0,	'a:18:{s:2:\"id\";i:3;s:4:\"u_id\";i:1;s:4:\"c_id\";i:3;s:4:\"code\";s:4:\"0008\";s:4:\"name\";s:8:\"MG面膜\";s:5:\"sales\";d:5;s:8:\"purchase\";d:1;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:1:\"4\";s:11:\"create_time\";i:1559019309;s:11:\"update_time\";i:1559019309;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"category\";s:6:\"美妆\";s:12:\"product_type\";s:6:\"正常\";}',	200.00),
(5,	5,	2,	3,	1,	2,	1559097009,	1,	NULL,	0,	'a:18:{s:2:\"id\";i:1;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:5:\"00001\";s:4:\"name\";s:6:\"苹果\";s:5:\"sales\";d:1;s:8:\"purchase\";d:1;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:1:\"1\";s:11:\"create_time\";i:1559019025;s:11:\"update_time\";i:1559019025;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"正常\";}',	2.00),
(6,	6,	2,	0,	1,	1,	1559097091,	1,	NULL,	0,	'a:18:{s:2:\"id\";i:1;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:5:\"00001\";s:4:\"name\";s:6:\"苹果\";s:5:\"sales\";d:1;s:8:\"purchase\";d:1;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:1:\"1\";s:11:\"create_time\";i:1559019025;s:11:\"update_time\";i:1559019025;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"正常\";}',	1.00),
(7,	7,	2,	1,	4,	5000,	1611933419,	1,	NULL,	0,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	110000.00),
(8,	8,	2,	1,	4,	1000,	1611933596,	1,	NULL,	0,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	22000.00),
(9,	9,	1,	0,	4,	1,	1611975264,	1,	NULL,	0,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	22.00),
(10,	10,	2,	0,	5,	11111,	1611975939,	1,	NULL,	0,	'a:20:{s:2:\"id\";i:5;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:4:\"1111\";s:4:\"name\";s:7:\"包材1\";s:5:\"image\";s:23:\"image/6014262fd6340.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:2;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611933266;s:11:\"update_time\";i:1611933266;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"赠品\";}',	244442.00),
(11,	11,	3,	0,	4,	5,	1611975966,	1,	NULL,	0,	'a:20:{s:2:\"id\";i:4;s:4:\"u_id\";i:1;s:4:\"c_id\";i:1;s:4:\"code\";s:3:\"111\";s:4:\"name\";s:6:\"牙刷\";s:5:\"image\";s:23:\"image/6014242b29a5f.jpg\";s:5:\"sales\";d:11;s:8:\"purchase\";d:22;s:6:\"points\";i:0;s:6:\"format\";s:0:\"\";s:6:\"lowest\";i:0;s:4:\"type\";i:1;s:4:\"unit\";s:0:\"\";s:11:\"create_time\";i:1611932729;s:11:\"update_time\";i:1611933130;s:10:\"update_uid\";i:1;s:6:\"remark\";s:0:\"\";s:8:\"bar_code\";s:0:\"\";s:8:\"category\";s:6:\"水果\";s:12:\"product_type\";s:6:\"成品\";}',	0.00);

DROP TABLE IF EXISTS `tb_product_supplier`;
CREATE TABLE `tb_product_supplier` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `company` varchar(255) DEFAULT NULL COMMENT '供应商',
  `name` varchar(255) DEFAULT NULL COMMENT '联系人',
  `tel` varchar(255) DEFAULT NULL COMMENT '电话',
  `fax` varchar(255) DEFAULT NULL COMMENT '传真',
  `mobile` varchar(255) DEFAULT NULL COMMENT '手机',
  `site` varchar(255) DEFAULT NULL COMMENT '网址',
  `email` varchar(255) DEFAULT NULL COMMENT 'EMAIL',
  `pc` varchar(255) DEFAULT NULL COMMENT '邮编',
  `address` varchar(255) DEFAULT NULL COMMENT '地址',
  `remark` text COMMENT '备注',
  `create_time` int(11) DEFAULT NULL COMMENT '创建日期',
  `update_time` int(11) DEFAULT NULL COMMENT '更新时间',
  `replace_uid` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `uid` (`u_id`),
  KEY `replace_uid` (`replace_uid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='供应商';

INSERT INTO `tb_product_supplier` (`id`, `u_id`, `company`, `name`, `tel`, `fax`, `mobile`, `site`, `email`, `pc`, `address`, `remark`, `create_time`, `update_time`, `replace_uid`) VALUES
(1,	1,	'包装盒厂家',	'333',	'33',	'33',	'33',	'',	'',	NULL,	'',	'',	1559019189,	1611980790,	1),
(2,	1,	'防伪标厂家',	'222',	'222',	'',	'',	'',	'',	NULL,	'',	'',	1559019196,	1611980785,	1),
(3,	1,	'苹果园',	'111',	'222',	'',	'',	'',	'',	NULL,	'',	'',	1559019205,	1611980781,	1),
(4,	1,	'测试',	'企业返现',	'1111111',	'',	'',	'',	'',	NULL,	'',	'',	1559054423,	1611980775,	1);

DROP TABLE IF EXISTS `tb_product_type`;
CREATE TABLE `tb_product_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

INSERT INTO `tb_product_type` (`id`, `title`) VALUES
(1,	'成品5'),
(2,	'赠品'),
(3,	'包材'),
(4,	'xxxx');

DROP TABLE IF EXISTS `tb_product_unit`;
CREATE TABLE `tb_product_unit` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '单位名称',
  `sort` int(11) DEFAULT '0' COMMENT '排序',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

INSERT INTO `tb_product_unit` (`id`, `name`, `sort`) VALUES
(1,	'个',	0),
(2,	'瓶',	0),
(3,	'箱',	0),
(4,	'片',	0),
(5,	'盒',	0);

DROP TABLE IF EXISTS `tb_product_warehouse`;
CREATE TABLE `tb_product_warehouse` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) DEFAULT NULL COMMENT '仓库名称',
  `default` int(1) DEFAULT '0' COMMENT '是否默认仓库',
  `address` varchar(255) DEFAULT NULL COMMENT '仓库地址',
  `remark` text,
  `sort` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='仓库';

INSERT INTO `tb_product_warehouse` (`id`, `name`, `default`, `address`, `remark`, `sort`) VALUES
(1,	'默认仓库',	1,	'',	'',	NULL),
(2,	'上海仓库',	1,	'',	'',	NULL),
(3,	'退货仓库',	0,	'',	'',	NULL);

DROP TABLE IF EXISTS `tb_product_warehouse_transfer`;
CREATE TABLE `tb_product_warehouse_transfer` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `jin_id` int(11) DEFAULT NULL COMMENT '拔入仓库',
  `out_id` int(11) DEFAULT NULL COMMENT '排出仓库',
  `p_id` int(11) DEFAULT NULL,
  `number` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `remark` varchar(500) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='调拨';

INSERT INTO `tb_product_warehouse_transfer` (`id`, `u_id`, `jin_id`, `out_id`, `p_id`, `number`, `create_time`, `remark`) VALUES
(1,	1,	1,	2,	1,	2,	1559054313,	''),
(2,	1,	2,	1,	4,	1,	1611984454,	'');

DROP TABLE IF EXISTS `tb_product_warehouse_user`;
CREATE TABLE `tb_product_warehouse_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `u_id` int(11) DEFAULT NULL,
  `w_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='仓库负责人';

INSERT INTO `tb_product_warehouse_user` (`id`, `u_id`, `w_id`) VALUES
(12,	1,	3),
(13,	31,	3),
(14,	1,	2),
(15,	31,	2),
(16,	1,	1),
(17,	31,	1);

DROP TABLE IF EXISTS `tb_session`;
CREATE TABLE `tb_session` (
  `session_id` varchar(150) NOT NULL,
  `session_expire` int(11) NOT NULL,
  `session_data` varchar(2000) NOT NULL,
  UNIQUE KEY `session_id` (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tb_session` (`session_id`, `session_expire`, `session_data`) VALUES
('think_upn3mo6qs6m04h44ts917fnsn1',	1611995429,	'think|a:0:{}');

DROP TABLE IF EXISTS `tb_system_menu`;
CREATE TABLE `tb_system_menu` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `pid` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `sort` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT NULL,
  `path` varchar(50) DEFAULT NULL,
  `icon` varchar(50) DEFAULT NULL,
  `url` varchar(100) DEFAULT NULL,
  `home` varchar(50) DEFAULT NULL,
  `is_dev` int(11) DEFAULT NULL,
  `rules` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='菜单表';

INSERT INTO `tb_system_menu` (`id`, `pid`, `name`, `sort`, `status`, `path`, `icon`, `url`, `home`, `is_dev`, `rules`) VALUES
(1,	0,	'系统首页',	1,	1,	'0-1',	'glyphicon glyphicon-home',	'index',	'',	0,	NULL),
(2,	1,	'控制台',	0,	1,	'0-1-2',	'',	'',	'',	0,	NULL),
(3,	2,	'首页',	1,	1,	'0-1-2-3',	'',	'admin/index/main',	'',	0,	'admin/admin/listorders,admin/index/index,admin/index/main'),
(4,	0,	'系统管理',	99,	1,	'0-4',	'glyphicon glyphicon-cog',	'system',	'',	0,	NULL),
(5,	4,	'系统设置',	1,	1,	'0-4-5',	'',	'',	'',	0,	NULL),
(6,	5,	'菜单管理',	3,	1,	'0-4-5-6',	'',	'admin/system/menu',	'',	0,	'admin/system/menu_rule_bind,admin/system/menu_show,admin/system/menu_hide,admin/system/menu,admin/system/menu_import,admin/system/menu_export,admin/system/menu_add,admin/system/menu_edit,admin/system/menu_delete'),
(7,	4,	'员工管理',	2,	1,	'0-4-7',	'',	'',	'',	0,	NULL),
(8,	7,	'员工管理',	1,	1,	'0-4-7-8',	'',	'admin/system/user',	'',	0,	'admin/system/user,admin/system/user_add,admin/system/user_edit,admin/system/user_password_reset,admin/system/user_delete'),
(10,	7,	'员工分组',	2,	1,	'0-4-7-10',	'',	'admin/system/auth_group',	'',	0,	'admin/system/auth_group,admin/system/auth_group_add,admin/system/auth_group_edit,admin/system/auth_group_delete'),
(12,	0,	'库存管理',	30,	1,	'0-12',	'glyphicon glyphicon-equalizer',	'inventory',	'',	0,	NULL),
(14,	12,	'库存管理',	3,	1,	'0-12-14',	'',	'',	'',	0,	NULL),
(15,	14,	'库存查询',	1,	1,	'0-12-14-15',	'',	'admin/inventory/stock_query',	'',	0,	'admin/configure/product_look,admin/inventory/stock_query,admin/inventory/transfer_add'),
(17,	12,	'入库管理',	1,	1,	'0-12-17',	'',	'',	'',	0,	NULL),
(18,	17,	'入库查询',	2,	1,	'0-12-17-18',	'',	'admin/inventory/storage_query',	'',	0,	'admin/inventory/storage_undo,admin/inventory/storage_query'),
(20,	17,	'入库',	1,	1,	'0-12-17-20',	'',	'admin/inventory/storage',	'',	0,	'admin/inventory/storage,admin/inventory/storage_submit,admin/json/product'),
(28,	57,	'产品管理',	6,	1,	'0-42-57-28',	'',	'admin/configure/product',	'',	0,	'admin/configure/product_look,admin/configure/product,admin/configure/product_add,admin/configure/product_edit,admin/configure/product_del,admin/res/index,admin/res/index_upload,admin/res/index_delete,admin/res/index_folder,admin/res/clear'),
(29,	14,	'调拨查询',	4,	1,	'0-12-14-29',	'',	'admin/inventory/transfer_query',	'',	0,	'admin/inventory/transfer_query'),
(31,	12,	'出库管理',	2,	1,	'0-12-31',	'',	'',	'',	0,	NULL),
(32,	31,	'出库',	1,	1,	'0-12-31-32',	'',	'admin/inventory/sales',	'',	0,	'admin/inventory/sales,admin/inventory/sales_submit,admin/json/product,admin/json/member'),
(33,	31,	'出库查询',	2,	1,	'0-12-31-33',	'',	'admin/inventory/sales_query',	'',	0,	'admin/inventory/sales_query,admin/inventory/sales_undo'),
(35,	31,	'退货查询',	4,	1,	'0-12-31-35',	'',	'admin/inventory/sales_returns_query',	'',	0,	'admin/inventory/sales_returns_query,admin/inventory/sales_returns_add,admin/inventory/sales_look,admin/inventory/sales_look_info_update'),
(37,	14,	'报废查询',	6,	1,	'0-12-14-37',	'',	'admin/inventory/scrapped_query',	'',	0,	'admin/inventory/scrapped_add,admin/inventory/scrapped_query'),
(38,	42,	'库存设置',	1,	1,	'0-42-38',	'',	'',	'',	0,	NULL),
(39,	57,	'产品分类',	1,	1,	'0-42-57-39',	'',	'admin/configure/product_category',	'',	0,	'admin/configure/product_category,admin/configure/product_category_add,admin/configure/product_category_delete,admin/configure/product_category_edit'),
(40,	38,	'仓库管理',	2,	1,	'0-4-38-40',	'',	'admin/configure/warehouse',	'',	0,	'admin/configure/warehouse,admin/configure/warehouse_add,admin/configure/warehouse_edit,admin/configure/warehouse_delete'),
(41,	38,	'计量单位',	3,	1,	'0-4-38-41',	'',	'admin/configure/unit',	'',	0,	'admin/configure/unit,admin/configure/unit_add,admin/configure/unit_edit,admin/configure/unit_delete'),
(42,	0,	'库存配置',	40,	1,	'0-42',	'glyphicon glyphicon-tasks',	'configure',	'',	0,	NULL),
(43,	42,	'会员管理',	3,	1,	'0-42-43',	'',	'',	'',	0,	NULL),
(44,	43,	'会员组',	2,	1,	'0-42-43-44',	'',	'admin/member/group',	'',	0,	'admin/member/group_price,admin/member/group,admin/member/group_add,admin/member/group_edit,admin/member/group_delete'),
(45,	43,	'会员列表',	3,	1,	'0-42-43-45',	'',	'admin/member/index',	'',	0,	'admin/member/index,admin/member/delete,admin/member/look,admin/member/edit,admin/member/add'),
(48,	38,	'供应商',	0,	1,	'0-42-47-48',	'',	'admin/configure/supplier',	'',	0,	'admin/configure/supplier,admin/configure/supplier_add,admin/configure/supplier_edit,admin/configure/supplier_look,admin/configure/supplier_delete'),
(52,	2,	'我的日志',	2,	1,	'0-1-2-52',	'',	'admin/index/log',	'',	0,	'admin/index/log_clear,admin/index/password,admin/index/log'),
(53,	38,	'快递管理',	4,	1,	'0-42-38-53',	'',	'admin/configure/express',	'',	0,	'admin/configure/express,admin/configure/express_add,admin/configure/express_edit,admin/configure/express_delete'),
(57,	42,	'产品管理',	2,	1,	'0-42-57',	'',	'',	'',	0,	NULL),
(59,	5,	'系统参数',	0,	1,	'0-4-5-59',	'',	'admin/system/config',	'',	0,	'admin/system/config'),
(60,	4,	'数据库',	3,	1,	'0-4-60',	'',	'',	'',	0,	NULL),
(61,	60,	'数据备份',	1,	1,	'0-4-60-61',	'',	'admin/database/export_list',	'',	0,	'admin/database/export,admin/database/export_list,admin/database/optimize,admin/database/repair'),
(62,	60,	'数据还原',	2,	1,	'0-4-60-62',	'',	'admin/database/import_list',	'',	0,	'admin/database/import_list,admin/database/del,admin/database/import'),
(63,	0,	'财务管理',	60,	1,	'0-63',	'glyphicon glyphicon-usd',	'finance',	'',	0,	NULL),
(65,	67,	'银行列表',	4,	1,	'0-63-64-65',	'',	'admin/finance/bank',	'',	0,	'admin/finance/bank,admin/finance/bank_add,admin/finance/bank_delete,admin/finance/bank_edit'),
(67,	63,	'财务',	1,	1,	'0-63-67',	'',	'',	'',	0,	NULL),
(68,	67,	'财务分类',	3,	1,	'0-63-67-68',	'',	'admin/finance/category',	'',	0,	'admin/finance/category,admin/finance/category_add,admin/finance/category_edit,admin/finance/category_delete'),
(70,	67,	'账务查询',	2,	1,	'0-63-67-70',	'',	'admin/finance/query',	'',	0,	'admin/finance/query,admin/finance/query_delete,admin/json/finance_category'),
(71,	67,	'新增财务',	1,	1,	'0-63-67-71',	'',	'admin/finance/add',	'',	0,	'admin/finance/add,admin/json/finance_category'),
(76,	80,	'包材关联',	3,	1,	'0-79-80-76',	'',	'admin/production/product_relation',	'',	0,	'admin/production/product_relation,admin/production/product_relation_edit,admin/production/product_relation_edit_submit'),
(77,	80,	'加工',	1,	1,	'0-79-80-77',	'',	'admin/production/product_build',	'',	0,	'admin/json/product,admin/production/product_build_submit,admin/production/product_build'),
(78,	80,	'加工记录',	2,	1,	'0-79-80-78',	'',	'admin/production/product_build_query',	'',	0,	'admin/production/product_build_undo,admin/production/product_build_query'),
(79,	0,	'生产管理',	2,	1,	'0-79',	'glyphicon glyphicon-scissors',	'production',	'',	0,	NULL),
(80,	79,	' 生产',	20,	1,	'0-79-80',	'',	'',	'',	0,	NULL),
(82,	0,	'统计报表',	90,	1,	'0-82',	'glyphicon glyphicon-signal',	'statistics',	NULL,	NULL,	NULL),
(83,	82,	'仓库',	0,	1,	NULL,	'',	'',	NULL,	NULL,	NULL),
(84,	83,	'出库报表',	0,	1,	NULL,	'',	'admin/statistics/sales',	NULL,	NULL,	'admin/statistics/sales'),
(85,	83,	'入库报表',	0,	1,	NULL,	'',	'admin/statistics/storage',	NULL,	NULL,	'admin/statistics/storage'),
(86,	5,	'系统附件',	86,	1,	'0-4-5-86',	'',	'admin/res/index',	NULL,	NULL,	'admin/res/index,admin/res/index_upload,admin/res/index_delete,admin/res/index_folder,admin/res/clear'),
(87,	82,	'财务',	87,	1,	'0-82-87',	'',	'',	NULL,	NULL,	NULL),
(88,	87,	'财务统计',	88,	1,	'0-82-87-88',	'',	'admin/statistics/finance',	NULL,	NULL,	'admin/statistics/finance'),
(89,	57,	'产品类型',	89,	1,	'0-42-57-89',	'',	'admin/configure/product_type',	NULL,	NULL,	'admin/configure/product_type,admin/configure/product_type_add,admin/configure/product_type_field,admin/configure/product_type_del');

DROP TABLE IF EXISTS `tb_system_user`;
CREATE TABLE `tb_system_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(20) NOT NULL,
  `nickname` varchar(20) NOT NULL,
  `password` varchar(32) NOT NULL,
  `email` varchar(50) DEFAULT NULL,
  `sex` tinyint(1) DEFAULT NULL,
  `status` tinyint(1) NOT NULL DEFAULT '0',
  `create_time` int(10) DEFAULT NULL,
  `update_time` int(10) DEFAULT NULL,
  `birthday` varchar(11) DEFAULT NULL,
  `mobile` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `username` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- 2021-01-30 07:30:37
