-- Adminer 4.6.3 MySQL dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;


ALTER TABLE `tb_member` ADD `addressee` varchar(255) NULL COMMENT '收件人';


DROP TABLE IF EXISTS `tb_product_purchase_order`;
CREATE TABLE `tb_product_purchase_order` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_number` varchar(255) DEFAULT NULL,
  `member_id` int(11) DEFAULT NULL,
  `create_time` int(11) DEFAULT NULL,
  `update_time` int(11) DEFAULT NULL,
  `total_price` decimal(10,2) DEFAULT '0.00',
  `total_quantity` int(11) DEFAULT '0',
  `remark` text,
  `status` int(11) DEFAULT '0' COMMENT '入库状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `tb_product_purchase_order_data`;
CREATE TABLE `tb_product_purchase_order_data` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_id` varchar(255) DEFAULT NULL,
  `supplier_id` int(11) DEFAULT '0' COMMENT '供应商',
  `product_id` int(11) DEFAULT '0' COMMENT '产品',
  `quantity` int(11) DEFAULT '0' COMMENT '数量',
  `group_price` decimal(10,2) DEFAULT '0.00' COMMENT '单价',
  `amount` decimal(10,2) DEFAULT '0.00' COMMENT '小计',
  `product_snapshot` text COMMENT '产品快照',
  `puts` int(11) DEFAULT '0' COMMENT '已放入仓库数量',
  `create_time` int(11) DEFAULT NULL,
  `status` int(11) DEFAULT '0' COMMENT '入库状态',
  PRIMARY KEY (`id`),
  KEY `order_id` (`order_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `tb_rel_purchase_storage`;
CREATE TABLE `tb_rel_purchase_storage` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `purchase_order_data_id` int(11) DEFAULT NULL COMMENT '采购订单order_data表id',
  `storage_id` int(11) DEFAULT NULL COMMENT '入库id',
  `storage_order_number` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


DROP TABLE IF EXISTS `tb_sales_type`;
CREATE TABLE `tb_sales_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `sort` varchar(200) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tb_sales_type` (`id`, `title`, `sort`) VALUES
(1,	'销售出库',	'0'),
(2,	'样品出库',	'0'),
(3,	'赠品出库',	'0'),
(4,	'退货出库',	'0'),
(5,	'换货出库',	'0'),
(6,	'次品补发',	'0'),
(9,	'其它出库',	'0');

DROP TABLE IF EXISTS `tb_storage_type`;
CREATE TABLE `tb_storage_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(200) NOT NULL,
  `sort` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `tb_storage_type` (`id`, `title`, `sort`) VALUES
(1,	'采购入库',	0),
(2,	'退货入库',	0),
(3,	'生产入库',	0),
(9,	'其它入库',	0);

-- 2021-09-08 06:40:23
