<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace app\admin\behavior;

use think\Db;

class Common extends \think\Controller {

    public function run($params) {

        $type = $params['type'];

        return $this->$type($params);
    }

    /**
     * 删除采购入库关联关系表
     * @param type $params
     * @return type
     */
    private function rel_purchase_storage_remove($params) {


        $storage_id = $params['storage_id'];


        Db::name('rel_purchase_storage')->where('storage_id', $storage_id)->delete();

        return true;
    }

}
