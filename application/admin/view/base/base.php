<!DOCTYPE html>
<html lang="zh-CN">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-urlA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- 上述3个meta标签*必须*放在最前面，任何其他内容都*必须*跟随其后！ -->
        <title></title>
        <!-- Set render engine for 360 browser -->
        <meta name="renderer" content="webkit">
        <script> var APP_URL = '<?php echo APP_URL; ?>';</script>  
        <script type="text/javascript" src="__PUBLIC__/libs/require.min.js"></script>
        <script type="text/javascript" src="__PUBLIC__/libs/require.config.js"></script>    
        <script type="text/javascript">
            requirejs(['bootstrap', 'scrollUp', 'jquery'], function (bootstrap, scrollUp, $) {
                $(function () {
                    $.scrollUp({scrollText: '回顶部'});
                });
            });
        </script>
        <!--[if lt IE 9]>
        <script src="__PUBLIC__/libs/html5shiv/3.7.2/html5shiv.min.js"></script>
        <script src="__PUBLIC__/libs/respond.js/1.4.2/respond.min.js"></script>
        <![endif]-->
        <link rel="stylesheet" href="__STATIC__/admin/css/style.css" />
        <style>
            img.img-thumbnail:hover{
                transform: scale(3);  
            }
        </style>
    </head>
    <body>
        <div class="container-fluid body">
            {block name="body"} {/block}
        </div>
        <div class="modal fade" tabindex="-1" role="dialog" id="modal">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->
        <div class="modal fade  bs-example-modal-lg" tabindex="-1" role="dialog" id="modal_big">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                </div><!-- /.modal-content -->
            </div><!-- /.modal-dialog -->
        </div><!-- /.modal -->

    </body>
</html>

<!--自定义的一些JS函数--> 
<script src="__STATIC__/admin/js/init.js"></script>
{block name="foot_js"}{/block}