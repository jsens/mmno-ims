{extend name="base:base" /}{block name="body"}  
<div class="table-common">
    <div class="left">
        <form class="form-inline" action="{:url('unit')}" method="get">
            <a data-toggle="modal" data-target="#modal" data-title="新增入库类型" href="<?php echo url('storage_type_add') ?>" title="新增入库类型" class="btn btn-default">
                <i class="glyphicon glyphicon-plus"></i> 新增入库类型</a>
        </form>
    </div>
</div>
<p>
    <small><i class="iconfont icon-tishi"></i> 查询到了<strong>{$lists|count}</strong>个入库类型</small>
</p>
{$tpl_list}
{/block}