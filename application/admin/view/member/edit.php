{extend name="base:base" /} {block name="body"}
<div class="table-common">
    <div class="left">
        <a class="btn btn-default" href="<?php echo url('member/index') ?>"><i class="glyphicon glyphicon-menu-left"></i> 返回列表</a>
        <button type="submit" class="btn btn-primary ajax-post" target-form="form-horizontal"><i class="glyphicon glyphicon-floppy-disk"></i> 保存</button>
    </div>
</div>
<form class="form-horizontal" action="{:url('edit')}" method="post">
    <input type="hidden" name="id" value="{$Think.get.id}" />
    {$tpl_form}
</form>
{/block}
{block name="foot_js"}
<!--加载时间框--> 
<script type="text/javascript" src="__PUBLIC__/libs/address-parse/dist/bundle.js"></script>
<script>
    
    require(['jquery'], function ($) {

        $("input[name='address']").blur(function () {


            var results = AddressParse.parse($(this).val());
            var mobile = results[0]['mobile'];
            if(mobile != null && mobile != "" && mobile != undefined){
                $(this).val(results[0]['province'] + results[0]['city'] + results[0]['area'] + results[0]['details'] );
                $("input[name='addressee']").val(results[0]['name']);
                $("input[name='tel']").val(results[0]['mobile']);
            }
        });

    });
    require(['jquery', 'datetimepicker'], function ($, datetimepicker) {
        $('#birthday').datetimepicker({lang: 'zh', format: 'Y-m-d', timepicker: false, closeOnDateSelect: true});
    });
</script> 
<!--加载时间框END-->
{/block}