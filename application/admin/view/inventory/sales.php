{extend name="base:base" /} {block name="body"}
<style>
    .table>tbody>tr>td,.table>tbody>tr>th{line-height: 30px;}
</style>
<form class="form-inline" action="{:url('sales')}"  method="post" autocapitalize="off" autocomplete="off" autocorrect="off">

    <table class="table table-hover">
        <tr>
            <th style="text-align:right"></th>
            <td>
                <div id="legend" class="text-ceter">
                    <h3>出库单</h3> 
                </div>
            </td>
        </tr>
        <tr>
            <th style="width:150px;text-align:right">选择会员</th>
            <td>
                <input type="text" class="form-control"  value="" id="automember" placeholder="会员名称搜索">
                <input type="hidden" id="member_id" name="member_id" />
            </td>
        </tr>
         <tr>
            <td colspan="2" id="member">
                
            </td>
        </tr>
         <tr>
            <th style="text-align:right">选择产品</th>
            <td style="">
                <input type="text"  placeholder="产品识别码或名称搜索" class="form-control" id="autoproduct">
             </td>
        </tr>
      
            <tr>
                <td colspan="2">
                    <table class="table table-hover table-striped" style="margin-bottom:0px">
                        <tbody id="products">

                        </tbody>
                    </table>

                </td>
            </tr>
            
        <tr>
            <th style="text-align:right">总金额</th>
            <td style=""><input class="form-control" id="productsummoney" name="amount" value="" type="number" /></td>
        </tr>
        <tr>
            <th style="text-align:right">发货日期</th>
            <td><input type="text" class="form-control" id="ship_time" name="ship_time" value="{$Think.post.ship_time?:date('Y-m-d H:i')}" placeholder="创建开始日期"></td>
        </tr>
        <tr>
            <th style="text-align:right">快递公司</th>
            <td>
                <select name="express_id" class="form-control">
                    <option value="">选择</option>
                    {volist name="express" id="var"}
                    <option value="{$var.id}" {eq name="Think.post.express_id" value="$var.id"} selected{/eq} >{$var.name}</option>
                    {/volist}
                </select>
                &nbsp;单号
                <input type="text" class="form-control" id="express_num" name="express_num" value="{$Think.get.express_num}" placeholder="快递单号" />
            </td>
        </tr>
        <tr>
            <th style="text-align:right">收货地址</th>
            <td>
                <input style="width: 70%" type="text" class="form-control" id="express_addr" name="express_addr" value="<?php
                $express_addr = input('post.express_addr', '');
                if (empty($express_addr)) {
                    echo isset($member['address']) ? $member['address'] : '';
                } else {
                    echo $express_addr;
                }
                ?>" placeholder="粘贴收货人信息，可自动识别" />
            </td>
        </tr>

        <tr>
            <th style="text-align:right">收件人</th>
            <td>
                <input  type="text" class="form-control" id="express_people" name="express_people" value="<?php
                $express_people = input('post.express_people', '');
                if (!empty($express_people)) {

                    echo $express_people;
                }
                ?>" placeholder="收件人" />
            </td>
        </tr>
        <tr>
            <th style="text-align:right">收件人电话</th>
            <td>
                <input  type="text" class="form-control" id="express_phone" name="express_phone" value="<?php
                $express_phone = input('post.express_phone', '');
                if (!empty($express_phone)) {

                    echo $express_phone;
                }
                ?>" placeholder="收件人电话" />
            </td>
        </tr>
        <tr>
            <th style="text-align:right">出库类型</th>
            <td>
                <select name="sales_type" class="form-control">
                    <?php echo html_select(db('sales_type')->column('id,title'), input('post.sales_type')); ?>
                </select>
            </td>
        </tr>
        <tr>
            <th style="text-align:right">出库备注</th>
            <td><textarea style="width: 20%" name="remark" type="" class="form-control" style="height:60px">{$Think.post.remark}</textarea></td>
        </tr>
        <tr>
            <td style="text-align:right"></td>
            <td colspan="5">
                <button type="submit" class="btn btn-primary ajax-post" target-form="form-inline" onclick="$('form').attr('action', '<?php echo url('sales_submit'); ?>');"><i class="glyphicon glyphicon-floppy-disk"></i> 保存</button>
            </td>
        </tr>
    </table>
</form>
{/block} 
{block name="foot_js"}

<script id="tpl-member" type="text/template">
    
    
    <table class="table table-hover " style="margin-bottom:0px">
        <tbody>
            <tr>
                <th style="width:150px;text-align:right">会员名称</th>
                <td style="width:250px;">{{member.nickname || ''}}</td>
                <th style="width:150px;text-align:right">会员分组</th>
                <td style="">{{member.name || ''}}</td>
            </tr>
        </tbody>
    </table>
    
    
</script> 

<script id="tpl-products" type="text/template">
    
    
 <tr id="tabletbody{{key}}"> 
    <td style="">识别码{{product.code}}</td>
    <td style="">条形码{{product.bar_code}}</td>
    <td style="">产品<input type="hidden" name="product_ids[{{key}}]" value="{{product.id}}" /> {{product.name}}</td>
    <td style="">出库价{{product.sales}}</td>                                    
    <td style="">数量
        <div class="form-group">
            <div class="input-group">
                <div class="input-group-addon lost" style="cursor:pointer" val="{{key}}" ><i class="glyphicon glyphicon-minus"></i></div>
                <input type="text"
                       style="width:80px;"
                       id="quantity{{key}}" class="quantity form-control text-center" sales="{{product.sales}}" 
                       name="product_quantity[{{key}}]" 
                       value="1" 
                       onkeyup="calculate_money({{key}})" 
                       placeholder="数量">
                <div class="input-group-addon just" style="cursor:pointer" val="{{key}}" ><i class="glyphicon glyphicon-plus"></i></div>
            </div>
        </div>
    </td>
    <td style="">实销价
        <input type="text" 
               id="group_price{{key}}"
               style="width:80px;" 
               name="group_price[{{key}}]"                                              
               value="{{product.group_price || product.sales}}" 
               placeholder="折扣价" 
               class="form-control group_price"
               onkeyup="calculate_money({{key}})"  >
    </td>
    <td style="">
        小计<input type="text" id="money{{key}}" key='{{key}}' value="{{product.group_price || product.sales}}" style="width: 100px" class="form-control money" disabled>
    </td>
    <td style="">
        仓库
        <select name="product_warehouse[{{key}}]" class="form-control" required>

            <option value="">选择</option>
            {{each product.warehouse vals key}}
            <option value="{{vals.id}}" <% if (vals.default) { %>selected<% } %> >{{vals.name}}[{{vals.quantity}}]</option>
            {{/each}}


        </select>                                      


    </td>
    <td>
        <input name="remarks[{{key}}]" class="form-control" 
               value=""
               placeholder="备注" />
    </td>
    <td><button type="button" class="btn btn-default remove" onclick="$('#tabletbody{{key}}').empty();sum();"><i class="glyphicon glyphicon-trash"></i></button></td>
</tr>   
    
    
</script>   

<script type="text/javascript" src="__PUBLIC__/libs/address-parse/dist/bundle.js"></script> 
<script>
require(['jquery', 'autocomplete', 'datetimepicker', 'template'], function ($, AutoComplete, datetimepicker, template) {        
        
        
    $(function () {
        $('#ship_time').datetimepicker(
                {lang: 'zh', format: 'Y-m-d H:i', timepicker: true, step: 5, closeOnDateSelect: true});
    });
 

    $("#express_addr").blur(function () {


        var results = AddressParse.parse($(this).val());
        var mobile = results[0]['mobile'];
        if(mobile != null && mobile != "" && mobile != undefined){
            $(this).val(results[0]['province'] + results[0]['city'] + results[0]['area'] + results[0]['details'] );
            $('#express_people').val(results[0]['name']);
            $('#express_phone').val(results[0]['mobile']);
        }
    });

    
 
    $('#automember').AutoComplete({
        'data': "<?php echo url('json/member') ?>",
        'ajaxDataType': 'json',
        'listStyle': 'normal',
        'maxItems': 10,
        'width': 'auto',
        'async': true,
        'matchHandler': function (keyword, data) {
            return true
        },
        'afterSelectedHandler': function (data) {
            $('#member_id').val(data.id);
           // $('form').submit();
           $.post("{:url('sales')}",{member_id:data.id},function(result){                
                $('#member').html(template('tpl-member', result.data));
                //
                $('#express_people').val(result.data.member.addressee);
                $('#express_phone').val(result.data.member.tel);
                $('#express_addr').val(result.data.member.address);
            });
        },
        'onerror': function (msg) {
            alert(msg);
        }
    });
    var key = 0;
    $('#autoproduct').AutoComplete({
        'data': "<?php echo url('json/product') ?>",
        'ajaxDataType': 'json',
        'listStyle': 'iconList',
        'maxItems': 10,
        'itemHeight': 55,
        'width': 300,
        'async': true,
        'matchHandler': function (keyword, data) {
            return true
        },
        'afterSelectedHandler': function (data) {
           // $('#product_id').val(data.id);
           // $('form').submit();
           
           $.post("{:url('sales')}",{product_id:data.id,member_id:$('#member_id').val()},function(result){
                result.data.key = ++key;
                $('#products').append(template('tpl-products', result.data));
                sum();
            });
           
           
        },
        'onerror': function (msg) {
            alert(msg);
        }
    });
 
 
    //动态计算
    function calculate_money(key) {
        var sub_price;
        var group_price = $('#group_price' + key).val();  //最终价
        var quantitynum = $('#quantity' + key).val(); //数量
        var money = $('#money' + key); //小合计   
        if (!quantitynum.match(new RegExp("^[0-9]+$")) || quantitynum <= 0) {
            $('#' + quantity).val('1');
            quantitynum = 1;
        }
        sub_price = (group_price * quantitynum).toFixed(2);
        money.val(sub_price);
        sum();
    }
    //求最终合计
    function sum() {
        var sumnum = 0;
        $('.money').each(function () {
            var key = $(this).attr('key');
            var value = $(this).val();
            var check = value.match(/[-+]?\d+/g);
            if (check != null) {
                sumnum = sumnum + Number(value);
            }
        });
        $('#productsummoney').val(sumnum.toFixed(2));
    }
    $(document).on('click','.just',function () {
        var key = $(this).attr('val');
        $('#quantity' + key).val(Number($('#quantity' + key).val()) + 1);
        calculate_money(key);
    });
    $(document).on('click','.lost',function () {
        var key = $(this).attr('val');
        var val = $('#quantity' + key).val();
        if (val > 1) {
            $('#quantity' + key).val((Number($('#quantity' + key).val()) - 1));
            calculate_money(key);
        }
    });
    
 });
</script>
{/block}