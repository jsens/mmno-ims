<?php

namespace app\admin\controller;

use app\admin\controller\Admin;
use think\Db;
use think\Session;
use utils\Date\Date;
use think\Config;

/**
 * @title 控制台
 */
class Index extends Admin {

    /**
     * @title 查看订单总额和子商品总额是否相同
     */
    public function order_check() {

        $lists = Db::name('product_sales_order')->order('id desc')->select();

        foreach ($lists as $key => $value) {


            // 子商品
            $lists2 = Db::name('product_sales_order_data')->where('o_id', $value['id'])->select();

            $test1 = 0;
            foreach ($lists2 as $key2 => $value2) {
                $test1 = $test1 + $value2['amount'];
            }

            if ($value['amount'] != $test1) {

                echo $value['id'] . ' --- ' . $value['amount'] . ':' . $test1 . '<br>';
            }
        }
    }

    /**
     * @title 日志删除
     */
    public function log_clear() {


        if ($affect_rows = model('operate')->clear()) {

            return $this->renderSuccess('清理了' . $affect_rows . '条数据 ', 'log');
        } else {
            return $this->renderError('没有清理任何数据');
        }
    }

    /**
     * @title 修改自己的密码
     */
    public function password() {

        if (request()->isPost()) {

            if (UID == 1)
                return $this->renderError('超级管理员暂不支持修改');



            $password = input('post.password');

            if (trim($password) == '') {
                return $this->renderError('密码不能为空');
            } else {

                if (Db::name('system_user')->where('id', UID)->update(['password' => my_md5($password)]) !== false) {
                    return $this->renderSuccess('更新成功');
                } else {
                    return $this->renderError('密码没有更新');
                }
            }
        } else {

            return view();
        }
    }

    /**
     * @title 框架页面
     */
    public function index() {

        //菜单列表
        $menu_list = model('system_menu')->getMenuList(UID);
        $this->assign('menu_list', json_encode($menu_list[1]));


        //菜单分组
        $menu_list_group = model('system_menu')->where('id', 'in', $menu_list[0])->order('sort asc')->select();
        $this->assign('menu_list_group', $menu_list_group);


        //用户的基本信息
        $this->assign('user_auth', Session::get('user_auth'));

        return view();
    }

    /**
     * @title 首页
     */
    public function main() {


        if (empty($_GET['timea']))
            $_GET['timea'] = date('Y-m-d', time() - 86400 * 30);
        if (empty($_GET['timeb']))
            $_GET['timeb'] = date('Y-m-d');


        // 出库
        $lists = model('product_sales_order')->model_where()->chart((Date::date_diff('d', $_GET['timea'], $_GET['timeb']) + 1));
        $this->assign('lists', $lists);

        // 入库
        $lists2 = model('product_storage_order')->model_where()->chart((Date::date_diff('d', $_GET['timea'], $_GET['timeb']) + 1));
        $this->assign('lists2', $lists2);


        // dd($lists2);
        // 磁盘使用率
        $df = @disk_free_space(".") / (1024 * 1024 * 1024);
        $dt = @disk_total_space(".") / (1024 * 1024 * 1024);

        $disk_per = round((1 - $df / $dt) * 100, 0);

        $this->assign('disk_per', $disk_per);

        return view();
    }

    /**
     * @title 我的日志
     */
    public function log() {

        if (!isset($_GET['timea']))
            $_GET['timea'] = date('Y-m-d', strtotime("-30 day"));
        if (!isset($_GET['timeb']))
            $_GET['timeb'] = date('Y-m-d');



        $count = model('operate')->model_where(UID)->count();
        $lists = model('operate')->model_where(UID)->paginate(20, $count);

        $this->assign('count', $count);
        $this->assign('lists', $lists);
        $this->assign('pages', $lists->render());



        return view();
    }

}
