<?php

namespace app\admin\controller;

use app\admin\controller\Base;
use think\Request;
use think\Db;
use think\Config;

/**
 * @title 打印
 */
class Prints extends Base {

    private $dbconfig;

    public function __construct(Request $request = null) {
        parent::__construct($request);
        
        $session_id = input('get.session_id', '');
        $session_data = Db::name('session')->where('session_id', config('session.prefix') . '_' . $session_id)->value('session_data');

        if (empty($session_data))
            exit('会话无效');

        $user_json = explode('|', $session_data);

        if (empty($user_json[1]))
            exit('用户无效');

        $user = unserialize($user_json[1]);

        $company = $user['company'] ?? 'default';

        $this->dbconfig = get_company_database();

        $this->assign('user_info', $user['user_auth']);

        define('UID', $user['user_auth']['id']);
        
        
        
    }

    /**
     * @title 出库订单详情
     */
    public function orders_view($id, $tpl = 'orders_view') {      
        
        empty($id) && exit();

        $var = Db::name('product_sales_order')
                        ->join('member m', 'm.id=a.m_id', 'LEFT')
                        ->join('system_user s', 'a.u_id=s.id', 'LEFT')
                        ->join('product_sales_order_data d', 'd.o_id=a.id', 'LEFT')
                        ->join('product p', 'd.p_id=p.id', 'LEFT')
                        ->join('express ex', 'ex.id = a.express_id', 'LEFT')
                        ->join('product_warehouse_user ws', 'd.w_id=ws.w_id', 'LEFT')
                        ->field('a.*,'
                                . 'm.nickname,'
                                . 's.nickname as staff_nickname,'
                                . 'COUNT(DISTINCT d.id) as count_data,'
                                . 'ex.name as express_name')
                        ->order('a.id desc')
                        ->alias('a')
                        ->where('ws.u_id', UID)
                        ->where('a.id', $id)->group('a.id')->find();
        //model('product_sales_order')->model_where(NULL)->where('a.id', $id)->group('a.id')->find();


        $this->assign('info', $var);

        // print_r($var);exit;

        if (!empty($var)) {
            Db::connect($this->dbconfig)->name('product_sales_order')->where('id', $id)->setField('print', 1);
            //  Db::connect($this->dbconfig)->name('operate')           
            //   model('operate')->success('打印出货单=>' . $var['order_number']);
        }

        $orders = Db::connect($this->dbconfig)->name('product_sales_order_data')->alias('psod')
                ->join('product_warehouse pw', 'pw.id=psod.w_id', 'LEFT')
                ->where('psod.o_id', $id)
                ->field('psod.*,pw.name as warehouse')
                ->select();


        $this->assign('orders', $orders);
        
        return view($tpl);
    }

    /**
     * @title 出库订单列表
     */
    public function orders_list() {

        if (!isset($_GET['timea']))
            $_GET['timea'] = date('Y-m-d', time() - 86400 * 30);
        if (!isset($_GET['timeb']))
            $_GET['timeb'] = date('Y-m-d');

        $this->assign('lists', model('product_sales_order_data')->model_where()->group('a.id')->select());
        model('operate')->success('打印出货单');
        return view();
    }

    /**
     * @title 入库查询
     */
    public function storage_list() {

        if (!isset($_GET['timea']))
            $_GET['timea'] = date('Y-m-d', time() - 86400 * 30);
        if (!isset($_GET['timeb']))
            $_GET['timeb'] = date('Y-m-d');

        $this->assign('lists', model('product_storage_order_data')->model_where()->select());
        model('operate')->success('打印入库单');
        return view();
    }

    /**
     * @title 入库查询
     */
    public function storage_view($id) {

        empty($id) && exit();


        if (!isset($_GET['timea']))
            $_GET['timea'] = date('Y-m-d', time() - 86400 * 30);
        if (!isset($_GET['timeb']))
            $_GET['timeb'] = date('Y-m-d');



        $var = Db::name('product_storage_order')
                        ->join('system_user su', 'a.u_id=su.id', 'LEFT')
                        ->join('product_supplier ps', 'a.s_id=ps.id', 'LEFT')
                        ->join('product_storage_order_data psod', 'psod.o_id=a.id', 'LEFT')
                        ->join('product_warehouse_user pwu', 'pwu.w_id=psod.w_id', 'LEFT')
                        ->join('product p', 'p.id=psod.p_id', 'LEFT')
                        ->field('a.*,su.nickname,ps.company')
                        ->order('a.id desc')
                        ->alias('a')
                        ->where('pwu.u_id', UID)
                        ->where('a.id', $id)->find();


        $sub_list = Db::connect($this->dbconfig)->name('product_storage_order_data')->join('product p', 'a.p_id=p.id', 'LEFT')
                        ->join('product_unit pu', 'pu.id=p.unit', 'LEFT')
                        ->join('product_warehouse pw', 'pw.id=a.w_id', 'LEFT')
                        ->join('product_warehouse_user pwu', 'a.w_id=pwu.w_id', 'LEFT')
                        ->join('product_supplier ps', 'a.s_id=ps.id', 'LEFT')
                        ->join('product_category pc', 'p.c_id=pc.id', 'LEFT')
                        ->join('product_inventory pi', 'a.p_id=pi.p_id and a.w_id=pi.w_id', 'LEFT')
                        ->join('system_user b', 'p.u_id=b.id', 'LEFT')
                        ->join('system_user c', 'p.update_uid=c.id', 'LEFT')
                        ->join('system_user e', 'a.u_id=e.id', 'LEFT')
                        ->field('a.*,'
                                . 'pi.quantity as inventory_quantity,'
                                . 'pc.name as category,'
                                . 'pw.name as warehouse,'
                                . 'ps.id as com_id,ps.company,'
                                . 'pu.name as unit_name,'
                                . 'p.name,p.code,p.purchase,'
                                . 'b.nickname,'
                                . 'c.nickname as replace_nickname,'
                                . 'e.nickname as storage_nickname')
                        ->order('a.id desc')
                        ->alias('a')
                        ->where('pwu.u_id', UID)
                        ->where('a.o_id', $var['id'])->select();


        $this->assign('sub_list', $sub_list);

        $this->assign('var', $var);

        // model('operate')->success('打印入库单');

        return view();
    }

    /**
     * @title 打印账务
     */
    public function finance_list() {

        if (!isset($_GET['timea']))
            $_GET['timea'] = date('Y-m-d', time() - 86400 * 30);
        if (!isset($_GET['timeb']))
            $_GET['timeb'] = date('Y-m-d');

        $this->assign('lists', model('finance_accounts')->model_where()->group('a.id')->select());
        model('operate')->success('打印账务单');
        return view();
    }

}
