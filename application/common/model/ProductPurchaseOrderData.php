<?php

namespace app\common\model;

use app\common\model\Base;
use Exception;
use think\Db;
use app\common\model\ProductStorageOrder;

class ProductPurchaseOrderData extends Base {

    public function getSupplierTextAttr($value, $data) {

        return Db::name('product_supplier')->where('id', $data['supplier_id'])->value('company');
    }

//    public function purchaseOrder() {
//        return $this->belongsTo('ProductPurchaseOrder');
//    }

    /**
     * 入库
     */
    public function storage($id, $post, $products) {



        Db::startTrans();
        try {


            // 执行入库操作
            $productStorageOrderModel = new ProductStorageOrder();
            $productStorageOrderModel->storage_submit($post, $products);
            if ($productStorageOrderModel->hasError()) {
                throw new \Exception($productStorageOrderModel->getError());
            }


            // 入库成功以后，更新订单的状态 -1 部分入库 -2 全部入库

            $this->update_status($post, $this->get($id));
            if ($this->hasError()) {
                throw new \Exception($this->getError());
            }

            // 写入log
            Db::name('rel_purchase_storage')->insert([
                'purchase_order_data_id' => $id,
                'storage_id' => $productStorageOrderModel->id,
                'storage_order_number' => $productStorageOrderModel->order_number,
            ]);



            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->setError($e->getMessage());
        }
    }

    /**
     * 入库操作
     * @param type $post
     * @param type $id  order_data id
     */
    public function update_status($post, $order_data) {


        Db::startTrans();
        try {

            // 更新入库数量
            Db::name('product_purchase_order_data')->where('id', $order_data['id'])->setInc('puts', $post['quantity']);



            // order
            Db::name('product_purchase_order')->where('id', $order_data['order_id'])->setField(['status' => -1]);

            Db::name('product_purchase_order_data')->where('id', $order_data['id'])->setField(['status' => -1]);

            // 读取最新order_data信息
            $one2 = Db::name('product_purchase_order_data')->where('id', $order_data['id'])->find();
            if ($one2['puts'] >= $one2['quantity']) {
                Db::name('product_purchase_order_data')->where('id', $order_data['id'])->setField(['status' => -2]);
            }
            // 如果一个订单下面所有记录都入库为-2，则主订单状态也要更新成-2
            if (!Db::name('product_purchase_order_data')->where('order_id', $order_data['order_id'])->where('status', '>=', -1)->find()) {
                Db::name('product_purchase_order')->where('id', $order_data['order_id'])->setField(['status' => -2]);
            }


            // 提交事务
            Db::commit();
        } catch (\Exception $e) {
            // 回滚事务
            Db::rollback();
            $this->setError($e->getMessage());
        }
    }

}
