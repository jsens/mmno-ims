<?php

namespace app\common\model;

use app\common\model\Base;

class ProductBuildOrderData extends Base {
    
    
    
    
    
    // 仓库名称
    public function getWarehouseTextAttr($value, $data) {
        return Db::name('product_warehouse')->where('id', $data['w_id'])->value('name');
    }
    
    
    // 产品名称
    public function getProductTextAttr($value, $data) {
        return Db::name('product')->where('id', $data['p_id_bc'])->value('name');
    }
    
    
    
    
    
    public function model_where(){
        
        $this->join('product_build_order pbo', 'pbo.id=a.o_id');
        $this->join('product p2', 'p2.id=pbo.p_id');
        $this->join('system_user su', 'su.id=pbo.u_id');
        
        $this->field('a.*,'
                . 'p2.name as product_title2,pbo.quantity as quantity2,'
                . 'pbo.build_time as build_time,'
                . 'su.nickname');
        
        $this->order('a.id desc');
        
        $this->alias('a');        
        return $this;
    }
    
}